import os.path
import json
import pytest

from QPySimu.utils import iosaves
from QPySimu.config import SimulationConfig


def test_save_simu_config():
    config = SimulationConfig(
        {"a": 10, "b": "hello", "c": 56.3},
        {"groupA": {"d": 29, "e": "Hello World"}}
    )
    path = 'tests/test_save_config.json'
    iosaves.save(path, config)
    assert os.path.exists(path)

    with open(path, 'r') as f:
        config_loaded = json.load(f)

    assert isinstance(config_loaded, dict)
    assert config_loaded == {
        'type': iosaves.TypesSerializable.SimulationConfig.value,
        'a': 10,
        'b': "hello",
        'c': 56.3,
        'groupA': {
            'd': 29,
            'e': "Hello World"
        }
    }


def test_load():
    obj = iosaves.load('tests/test_save_config.json')
    assert isinstance(obj, SimulationConfig)
    assert obj['a'] == 10
    assert obj['b'] == 'hello'
    assert obj['groupA'] == {
        'd': 29,
        'e': "Hello World"
    }
