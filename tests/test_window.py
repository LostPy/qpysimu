from QPySimu.gui import BaseSimuWindow
from QPySimu.core import Simulation

import pytest


def test_base_window(qtbot):
    w = BaseSimuWindow("config-example.yml")
    simulation = Simulation(10, 0.1, parent=w)
    w.setSimulation(simulation)
    qtbot.addWidget(w)
    assert w.windowTitle() == 'test'
    assert len(w.parameters_widgets) == 1
    assert len(w.group_parameters_widgets) == 1
