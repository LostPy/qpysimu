import pytest
from time import time_ns, sleep

from PySide6.QtWidgets import QGraphicsLineItem

from QPySimu.config import SimulationConfig
from QPySimu.core import (
    Simulation,
    SimulationResult,
    SimuState,
    SimulationMode,
)
from QPySimu.items import ItemDisplayable
from QPySimu.utils import iosaves


class Line(ItemDisplayable):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def default_graphics_item(cls) -> QGraphicsLineItem:
        return QGraphicsLineItem(1, 1, 2, 2)


iosaves.add_item_types([Line])


def test_simu_item():
    item = Line()
    assert item.is_visible is True
    assert isinstance(item.graphics_item, QGraphicsLineItem)


def test_simu_state():
    items = [
        Line(init_pos=(i, 2 * i))
        for i in range(5)
    ]
    state = SimuState(items, a=3, b=19, c='Hello World')
    assert len(state.displayable_items) == 5
    assert state['a'] == 3
    assert state['b'] == 19
    with pytest.raises(KeyError):
        state['d']


def test_simulation(qtbot):

    items = [Line(), Line(init_pos=(1, 1))]

    state = SimuState(items, speed=1)

    def step_fct(state: SimuState, config: SimulationConfig) -> SimuState:
        for item in state.displayable_items.values():
            item.x += 2
            item.y += 3
        return state

    simulation = Simulation(10, 1,
                            step_function=step_fct, initial_state=state)

    with qtbot.waitSignal(simulation.finished, timeout=5000) as blocker:
        simulation.start(SimulationConfig({}, {}))

    assert items[0].x == 20 and items[0].y == 30
    assert items[1].x == 21 and items[1].y == 31


def test_simulation_result(qtbot):
    simu_result = SimulationResult(frequency=100)

    assert simu_result.save_file == './states.qpys'
    assert simu_result.frequency == 100
    assert simu_result.mode == SimulationMode.STOP

    simu_result.setMode(SimulationMode.RUN)
    assert simu_result.mode == SimulationMode.RUN

    with qtbot.waitSignal(simu_result.finished, timeout=5000) as blocker:
        simu_result.start()
        sleep(0.5)
        simu_result.setMode(SimulationMode.STOP)
    assert simu_result.mode == SimulationMode.STOP
    assert not simu_result.isRunning()
