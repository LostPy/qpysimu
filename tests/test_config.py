import sys
from pathlib import Path
from yaml import safe_load

sys.path.insert(0, str(Path(__file__).parent.absolute().parent))

from QPySimu.config import (
    Parameter,
    NumberParameter,
    GroupParameters,
    create_parameter,
    SimuInterfaceConfig,
    AppConfig
)

import pytest


with open('tests/test_config.yml', 'r') as f:
    TEST_CONFIG = safe_load(f)


def test_parameter():
    param = Parameter("test", str)
    assert param.name == "test"
    assert param.param_type == str
    assert param.description is None
    assert param.default is None


def test_number_param():
    param = NumberParameter("test",
                            int,
                            default=5,
                            description="desc",
                            _range=(0, 10))
    assert param.name == "test"
    assert param.param_type == int
    assert len(param.description) == 4
    assert param.default == 5
    assert param.min == 0
    assert param.max == 10


def test_create_parameter(param: Parameter = None):
    if param is None:
        param = create_parameter(
            'speed',
            TEST_CONFIG['simulation']['parameters']['speed'])
    assert param.name == 'speed'
    assert param.param_type == float
    assert param.default == 1
    assert param.min == 0.2
    assert param.max == 5


def test_simu_config():
    simu_config = SimuInterfaceConfig(TEST_CONFIG['simulation'])
    assert simu_config.minimap is True
    assert simu_config.zoom is False
    assert isinstance(simu_config.get('mouse'), GroupParameters)
    test_create_parameter(simu_config.parameters['speed'])
