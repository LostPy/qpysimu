import pytest

from QPySimu.gui.widgets import (
    create_parameter_widget,
    NumberParameterWidget,
    ChoiceParameterWidget,
)
from QPySimu.config import NumberParameter, ChoiceParameter


def test_int_param(qtbot):
    param = NumberParameter("Integer", int, 0, _range=(-20, 60))
    widget = create_parameter_widget(param)
    qtbot.addWidget(widget)
    assert isinstance(widget, NumberParameterWidget)
    assert widget.get_value() == 0
    assert widget.valueWidget.minimum() == -20
    assert widget.valueWidget.maximum() == 60


def test_float_param(qtbot):
    param = NumberParameter("Float", float, description="Some float")
    widget = create_parameter_widget(param)
    qtbot.addWidget(widget)
    assert isinstance(widget, NumberParameterWidget)
    assert widget.valueWidget.toolTip() == "Some float"
    assert widget.get_value() == 0
    assert widget.valueWidget.minimum() == NumberParameterWidget.DEFAULT_MINIMUM
    assert widget.valueWidget.maximum() == NumberParameterWidget.DEFAULT_MAXIMUM


def test_choice_param(qtbot):
    param = ChoiceParameter("choices", choices=["a", "b", "c"])
    widget = create_parameter_widget(param)
    qtbot.addWidget(widget)
    widget.valueWidget.setCurrentIndex(0)
    assert isinstance(widget, ChoiceParameterWidget)
    assert len(widget.parameter.choices) == 3 and widget.valueWidget.count() == 3
    assert widget.get_value() == "a"
