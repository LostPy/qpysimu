from PySide6.QtWidgets import QApplication, QGraphicsLineItem

from QPySimu.config import SimulationConfig
from QPySimu.gui import SimuWindow2D
from QPySimu.core import Simulation, SimuState
from QPySimu.items import ItemDisplayable

import time


class Line(ItemDisplayable):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def default_graphics_item(cls) -> QGraphicsLineItem:
        return QGraphicsLineItem(0, 0, 100, 150)


def step(state: SimuState, config: SimulationConfig):
    for item in state.displayable_items.values():
        item.x += 10
        item.y += 5
    time.sleep(0.02)
    return state


items = [
    Line(init_pos=(i * 5, 10 * i))
    for i in range(5)
]

app = QApplication([])
w = SimuWindow2D("config-example.yml", [Line])
simu = Simulation(
    50,
    0.1,
    step_function=step,
    initial_state=SimuState(items),
    parent=w
)
w.setSimulation(simu)
w.show()
app.exec()
