from . import QPySimuException


class ConfigExeption(QPySimuException):

    """Exception raised when a error was found during the parse of config file.
    """

    def __init__(self, msg: str = None):
        super().__init__(msg)

    def __repr__(self) -> str:
        if self.msg:
            return super().__repr__()
        return "A error was detected in the config file."


class ParamTypeUnsupported(ConfigExeption):

    """Exception raised when the a type of parameter is not supported.
    """

    def __init__(self, type_name: str):
        super().__init__()
        self.msg = f"The type '{type_name}' is unsupported."
