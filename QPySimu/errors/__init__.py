
class QPySimuException(Exception):

    """Base class for exceptions of this package

    Attributes
    ----------
    msg : str
        A optional message to display
    """

    def __init__(self, msg: str = None):
        super().__init__()
        self.msg = msg

    def __repr__(self) -> str:
        if self.msg:
            return self.msg
        return ""


from .config import ConfigExeption, ParamTypeUnsupported
from .iosaves import ItemClassNotFound
