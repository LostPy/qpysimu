from . import QPySimuException


class ItemClassNotFound(QPySimuException):
    """Exception raised while reading a json file
    containing an object of unknown class
    (class not present in the "CLASS_DISPLAYABLE_ITEMS" list).
    """
    pass
