from typing import Any
from copy import deepcopy

from QPySimu.items import ItemDisplayable


class SimuState:
    """Class to represent a "State" of simulation,
    e.g., all variables (including ItemDisplayable) at a time.

    Attributes
    ----------
    displayable_items : dict[int, ItemDisplayable]
        A dictionary with the id of displayable items \
        as keys and displayable items as values
    properties : dict[str, Any]
        A dictionary with properties (variables) of the simulation
    """

    def __init__(self, displayable_items: list[ItemDisplayable], **kwargs):
        """Initialize an instance of SimuState

        Parameters
        ----------
        displayable_items : list[ItemDisplayable]
            The list of displayable items.
        **kwargs
            Other variables of the simulation
        """
        self.displayable_items: dict[int, ItemDisplayable] = {
            item.id: item
            for item in displayable_items
        }
        self.properties: dict[str, Any] = kwargs

    def __getitem__(self, k: str) -> Any:
        """Get the value of a variable with the name,
        with the syntax `state['name']`.
        Raised a KeyError if the variable does not exist.

        Parameters
        ----------
        k : str
            Variable's name

        Returns
        -------
        Any
            The value of the variable
        """
        return self.properties[k]

    def __setitem__(self, k: str, v: Any):
        """Set a new variable or update a variable,
        with the syntax `state['name'] = value`

        Parameters
        ----------
        k : str
            Variable's name
        v : Any
            New value of the variable
        """
        self.properties[k] = v

    def get(self, k: str) -> Any:
        """Get the value of a variable with the name.
        Return None if the variable does not exist.

        Parameters
        ----------
        k : str
            Variable's name

        Returns
        -------
        Any
            Variable's value
        """
        return self.properties.get(k)

    def to_dict(self) -> dict[str, Any]:
        """Returns displayable items and variables in a dictionary.
        Use to save the state in a json format.

        Returns
        -------
        dict[str, Any]
            The dictionary with displayable items and variables.
        """
        return {
            'displayable_items': self.displayable_items,
            'properties': self.properties
        }

    @classmethod
    def from_dict(cls, d: dict[str, Any]) -> 'SimuState':
        """Create an instance of SimuState with a dictionary
        created by the to_dict method.

        Parameters
        ----------
        d : dict[str, Any]
            The dictionary with SimuState data

        Returns
        -------
        SimuState
            The new instance initialized with dictionary
        """
        return cls(d['displayable_items'].values(), **d['properties'])
