import os
from shutil import rmtree

from PySide6.QtCore import QThread, Signal

from QPySimu.config import SimulationConfig
from QPySimu.utils import tar, iosaves

from . import SimuState


class Simulation(QThread):

    """Class to define a simulation,
    with a repeat algorithm for a given duration with a given time step.
    It's a subclass of QThread.

    !!! note

        It is recommended to create a subclass of Simulation by redefining \
        the step method which should contain the simulation algorithm.

        However, it is possible to write the algorithm in a function and \
        pass this function at the creation of the Simulation instance 
        using the 'step_function' argument.

    !!! info

        The function containing the algorithm and the step method must take \
        an instance of SimuState as argument (state t-1) and \
        returns an instance of SimuState (state t).

    ??? example "Example of subclass"

        ```python
        class MySimulation(Simulation)

            def __init__(self, time: int, dt: float, **kwargs):
                super().__init__(time, dt, **kwargs)

            def step(self):
                for item in self.state.displayable_items.values():
                    # move item
                    item.x += 2
                    item.y += 2
        ```

    Attributes
    ----------
    FILE_STATE_TEMPLATE : str
        Class attribute to define the save filename template.
    TMP_SAVE_DIR : str
        Class attribute to define the path of temporary save directory.
    progressed : Signal
        Signal emitted at each iteration of the simulation.
    time : int
        The duration of the simulation
    dt : float
        The time step to be used
    config : SimulationConfig
        An instance of SimulationConfig
    state : SimuState
        The current state of the simulation
    save_file : str
        The filename where states are saved.
    step_function : function
        The function with the algorithm.
        To be used if you do not create a subclass

    !!! info

        The step_function must take an instance of SimuState as argument \
        (state t-1) and return an instance of SimuState (state t).
    """

    FILE_STATE_TEMPLATE = "state-t{t}.json"
    TMP_SAVE_DIR = ".tmp-qpysimu-states"

    progressed = Signal(float, float)

    def __init__(self, time: int, dt: float, *,
                 step_function=None, initial_state: SimuState = None,
                 save_file: str = './states.qpys', parent=None):
        """Initialize an instance of Simulation

        Parameters
        ----------
        time : int
            The duration of the simulation
        dt : float
            The time step to be used
        step_function : None, optional
            The function with the algorithm.
            To be used if you do not create a subclass
        initial_state : SimuState, optional
            Description
        save_file : str, optional
            The filename where states are saved.
        parent : None, optional
            The QObject parent
        """
        super().__init__(parent)
        self.time = time
        self.dt = dt
        self.config = None  # Initialized in the start method
        self.state = initial_state
        self.save_file = save_file
        self.step_function = step_function
        self.finished.connect(self.on_finished)

    def start(self, config: SimulationConfig,
              priority: QThread.Priority = QThread.InheritPriority):
        """Method called to start the simulation (QThread).

        Parameters
        ----------
        config : SimulationConfig
            The configuration for the simulation.
        priority : QThread.Priority, optional
            Priority of the QThread.

        Raises
        ------
        ValueError
            Raised if 'config' isn't an instance of SimulationConfig.
        """
        if self.step_function is None:
            raise ValueError("'step_function' must be defined")
        if not isinstance(config, SimulationConfig):
            raise ValueError(
                "'config' must be an instance of 'SimulationConfig', "
                f"not an instance of '{type(config)}'.")
        if not os.path.exists(self.TMP_SAVE_DIR):
            os.mkdir(self.TMP_SAVE_DIR)
        self.config = config
        super().start(priority)

    def on_finished(self):
        """Method called when 'finished' signal is called.
        Create the archive with all states saved and remove the tmp dir.
        """
        tar.create_archive(self.save_file,
                           os.listdir(self.TMP_SAVE_DIR),
                           dir_=self.TMP_SAVE_DIR)
        rmtree(self.TMP_SAVE_DIR)

    def run(self):
        """Mehtod executed with start method.
        This method takes care of the iterations and calls the step method.
        """
        t = 0
        self._save_current_state(t)  # save state at t=0
        while t < self.time:
            t += self.dt
            self.step()
            self._save_current_state(t)
            self.progressed.emit(round(t, 2), round(t / self.time, 2))

    def step(self):
        """Method where the algorithm is executed, update the state of simulation.
        This method should be define in subclass.
        """
        self.state = self.step_function(self.state, self.config)

    def _save_current_state(self, t: float):
        """Method to save the current state at the time given (t)

        Parameters
        ----------
        t : float
            The current time of the simulation.
        """
        filename = self.FILE_STATE_TEMPLATE.format(
            t=str(round(t, 2)).replace('.', '_'))
        iosaves.save(
            f"{self.TMP_SAVE_DIR}/{filename}",
            {
                'time': self.time,
                'dt': self.dt,
                't': round(t, 2),
                'config': self.config,
                'state': self.state
            }
        )
