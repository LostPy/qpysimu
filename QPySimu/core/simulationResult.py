import os
from shutil import rmtree
from glob import glob
from queue import LifoQueue
from enum import Enum
from time import sleep

from PySide6.QtCore import QThread, Signal

from QPySimu.utils import tar, iosaves

from . import SimuState, Simulation


class SimulationMode(Enum):

    """Enumeration of mode for the SimulationResult class.

    Attributes
    ----------
    STOP : int
        When the simulation don't run
    RUN : int
        When the simulation is running automatically.
    PAUSE : int
        When the simulation is paused
    STEP_BY_STEP : int
        When the simulation is running manually.
    """

    STOP = 0
    RUN = 1
    PAUSE = 2
    STEP_BY_STEP = 3


class SimulationResult(QThread):

    """Class to read the result of a simulation.
    It's a subclass of QThread

    Attributes
    ----------
    TMP_RESULT_DIR : str
        The temporary directory used to load a simulation
    progressed : Signal
        Signal emitted when a new state is loaded
    modeChanged : Signal
        Signal emitted when the mode changed
    save_file : str
        The save file of the simulation
    frequency : int
        Define the speed of simulation
    files : list[str]
        The list of file for states (SimuState) to loaded
    historic_files : queue.LifoQueue
        Files already read
    review_files : queue.LifoQueue
        Files skip backwars, removed from historic_files
    mode : SimulationMode
        The current mode of the simulation review
    """

    TMP_RESULT_DIR = ".tmp-qpysimu-result"

    progressed = Signal(float, int, float, SimuState)
    modeChanged = Signal(SimulationMode)

    def __init__(self, save_file: str = './states.qpys',
                 frequency: int = 30, parent=None):
        """Initialize an instance of SimulationResult

        Parameters
        ----------
        save_file : str, optional
            The save file of the simulation
        frequency : int, optional
            Define the speed of simulation
        parent : None, optional
            The QObject parent
        """
        super().__init__(parent)
        self.files: list[str] = list()  # init with states_dir
        self.historic_files: LifoQueue = LifoQueue()
        self.review_files: LifoQueue = LifoQueue()
        self.save_file = save_file
        self.frequency = frequency
        self._mode = SimulationMode.STOP
        self._next = False
        self._previous = False
        self.finished.connect(self.on_finished)

    @property
    def mode(self) -> SimulationMode:
        """Current mode of the simulation review

        Returns
        -------
        SimulationMode
            The current mode
        """
        return self._mode

    @mode.setter
    def mode(self, new: SimulationMode):
        """Update the mode

        Parameters
        ----------
        new : SimulationMode
            The new mode

        Raises
        ------
        ValueError
            Exception raised if mode is not an instance of SimulationMode.
        """
        if not isinstance(new, SimulationMode):
            raise ValueError("'mode' must be a value of SimulationMode, "
                             f"not an instance of '{type(new)}'.")
        self._mode = new
        self.modeChanged.emit(self._mode)

    def _sort_files(self):
        """Method to sort the files in chronological order in the simulation.
        """
        parts_template = Simulation.FILE_STATE_TEMPLATE.split('{t}')
        before_nb = parts_template[0]
        after_nb = parts_template[1]

        def number_from_filename(file: str) -> float:
            nb = file[
                file.find(before_nb) + len(before_nb):file.find(after_nb)
            ]
            return float(nb.replace('_', '.'))

        self.files = sorted(self.files, key=number_from_filename)

    def _get_files(self):
        """Method to get files with the  simulation result data.
        """
        filename_template = os.path.join(
            self.TMP_RESULT_DIR,
            Simulation.FILE_STATE_TEMPLATE.replace('{t}', '*'))
        self.files = glob(filename_template)
        self._sort_files()

    def start(self, priority: QThread.Priority = QThread.InheritPriority):
        """Method called to start the simulation (QThread).
        Extract the archive file and reset the temporary directory.

        Parameters
        ----------
        priority : QThread.Priority, optional
            Priority of the QThread.
        """
        if os.path.exists(self.TMP_RESULT_DIR):
            rmtree(self.TMP_RESULT_DIR)
        os.mkdir(self.TMP_RESULT_DIR)
        tar.extract_archive(self.save_file, self.TMP_RESULT_DIR)
        self._get_files()
        self.historic_files = LifoQueue()
        self.review_files = LifoQueue()
        super().start(priority)

    def on_finished(self):
        """Method called when the finished Signal is emitted.
        Remove the temporary directory.
        """
        rmtree(self.TMP_RESULT_DIR)

    def run(self):
        """Main method of QThread.
        Performs the actions for each mode.
        """
        while self._mode != SimulationMode.STOP and len(self.files) > 0:
            if self._mode == SimulationMode.RUN or\
                    (self._mode == SimulationMode.STEP_BY_STEP and self._next):
                if self.review_files.empty():
                    file = self.files.pop(0)
                else:
                    file = self.review_files.get()
                time, dt, t, config, state = self.step(file)
                self.historic_files.put(file)
                self.progressed.emit(t, time, round(t / time, 2), state)

            elif self._mode == SimulationMode.STEP_BY_STEP and self._previous\
                    and not self.historic_files.empty():
                file = self.historic_files.get()
                time, dt, t, config, state = self.step(file)
                self.review_files.put(file)
                self.progressed.emit(t, time, round(t / time, 2), state)
                self._previous = False

            if self._next:
                self._next = False
            sleep(1 / self.frequency)

    def step(self, file: str) -> tuple:
        """A step of run method:
        Read save states files and returs

        Parameters
        ----------
        file : str
            The save file to read

        Returns
        -------
        tuple[int, float, float, SimulationConfig, SimuState]
            Returns (time, dt, t, config, state)
        """
        save = iosaves.load(file)
        time = save['time']
        dt = save['dt']
        t = save['t']
        config = save['config']
        state = save['state']
        return time, dt, t, config, state

    def setMode(self, mode: SimulationMode):
        """Update the mode

        Parameters
        ----------
        mode : SimulationMode
            The new mode to use
        """
        self.mode = mode

    def next(self):
        """Changes to the next state.
        Use in STEP_BY_STEP mode.
        """
        self._next = True

    def previous(self):
        """Changes to the previous state.
        Use in STEP_BY_STEP mode.
        """
        self._previous = True
