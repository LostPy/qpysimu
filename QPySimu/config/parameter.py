from typing import Any, Union

from QPySimu import errors


class BaseParameter:

    """Base class for parameters of simulation.

    Attributes
    ----------
    description : str
        The parameter's description
    """

    def __init__(self, name: str, description: str = None):
        """Initialize an instance of BaseParameter

        Parameters
        ----------
        name : str
            The parameter's name
        description : str, optional
            The parameter's description
        """
        self._name: str = name
        self.description: str = description

    @property
    def name(self) -> str:
        """The parameter's name

        Returns
        -------
        str
            The parameter's name
        """
        return self._name


class Parameter(BaseParameter):

    """Class to define a parameter with type and default value.
    """

    def __init__(self,
                 name: str,
                 _type: type,
                 default: 'self.param_type' = None,
                 description: str = None):
        """Initialize a parameter

        Parameters
        ----------
        name : str
            The parameter's name
        _type : type
            The type of parameter
        default : self.param_type, optional
            The default value
        description : str, optional
            The parameter's description

        Raises
        ------
        ValueError
            Raised ValueError if the default value \
            is not of the type specified by "_type"
        """
        super().__init__(name, description)
        self._type = _type
        if default is not None and not isinstance(default, self._type):
            raise ValueError(
                f"'default' value must be an instance of '{self._type}'")
        self._default = default

    @property
    def param_type(self) -> type:
        """The type of parameter

        Returns
        -------
        type
            Type of parameter
        """
        return self._type

    @property
    def default(self) -> 'self.param_type':
        """The default value of parameter

        Returns
        -------
        self.param_type
            The default value
        """
        return self._default


class NumberParameter(Parameter):

    """Class to define a numeric parameter (float or int).
    """

    def __init__(self,
                 name: str,
                 _type: type,
                 default: Union[int, float] = None,
                 description: str = None,
                 _range: tuple = (None, None)):
        """Initialize an instance of NumberParameter

        Parameters
        ----------
        name : str
            Parameter's name
        _type : type
            The parameter's type (int or float)
        default : Union[int, float], optional
            The default value
        description : str, optional
            Parameter's description
        _range : tuple[Union[int, float], Union[int, float]], optional
            The range of value (minimum, maximum)
        """
        super().__init__(name,
                         _type,
                         _type(default) if default is not None else None,
                         description)
        self._range = _range

    @property
    def range(self) -> tuple:
        """The range of parameter's value

        Returns
        -------
        tuple[Union[int, float], Union[int, float]]
            Range of parameter's value (minimum, maximum)
        """
        return self._range

    @property
    def min(self) -> Union[int, float]:
        """The minimum value of parameter

        Returns
        -------
        Union[int, float]
            Min value of parameter: `self.range[0]`
        """
        return self._range[0]

    @property
    def max(self) -> Union[int, float]:
        """The maximum value of parameter.

        Returns
        -------
        Union[int, float]
            Max value of parameter: `self.range[1]`
        """
        return self._range[1]


class StringParameter(Parameter):

    """Class to define a string parameter.
    """

    def __init__(self,
                 name: str,
                 default: str = None,
                 description: str = None):
        """Initialize an instance of StringParameter

        Parameters
        ----------
        name : str
            Parameter's name
        default : str, optional
            The default value of parameter
        description : str, optional
            Parameter's description
        """
        super().__init__(name, str, default, description)


class ChoiceParameter(StringParameter):

    """Class to define a string parameter with a list of choices.
    """

    def __init__(self,
                 name: str,
                 choices: list[str],
                 default: str = None,
                 description: str = None):
        """Initialize an instance of ChoiceParameter

        Parameters
        ----------
        name : str
            Parameter's name
        choices : list[str]
            Choices of value
        default : str, optional
            The default value of parameter
        description : str, optional
            Parameter's description
        """
        super().__init__(name, default, description)
        self._choices = choices

    @property
    def choices(self) -> list[str]:
        """Choices of value for this parameter

        Returns
        -------
        list[str]
            The choices list
        """
        return self._choices

    def add_choice(self, choice: str):
        """Add a choice to the choices list.

        Parameters
        ----------
        choice : str
            The choice to add
        """
        self._choices.append(str(choice))


class GroupParameters(BaseParameter):

    """Class to define a group of parameters.

    Attributes
    ----------
    parameters : dict[str: Parameter]
        Parameters of the group
    """

    def __init__(self, name: str, parameters: list[Parameter]):
        """Initialize a group parameters

        Parameters
        ----------
        name : str
            Group's name
        parameters : list[Parameter]
            The parameters list of the group
        """
        super().__init__(name)
        self.parameters = {
            param.name: param
            for param in parameters
        }

    def __len__(self) -> int:
        """parameters number

        Returns
        -------
        int
            Size of the parameters dict
        """
        return len(self.parameters)

    def __getitem__(self, key: str) -> Any:
        """Get a parameter with the name

        Parameters
        ----------
        key : str
            Parameter's name to get

        Returns
        -------
        Any
            The Parameter instance
        """
        return self.parameters[key]

    def get(self, key: str) -> Any:
        """Get a parameter with the name

        Parameters
        ----------
        key : str
            Parameter's name to get

        Returns
        -------
        Any
            The Parameter instance
        """
        return self.parameters.get(key)


def _str_to_pytype(_str: str) -> type:
    """Function to convert a type name (str) in Python type

    Parameters
    ----------
    _str : str
        The name of type

    Returns
    -------
    type
        The type corresponding to the name

    Raises
    ------
    errors.ParamTypeUnsupported
        Exception raised when the name is not a supported type
    """
    if _str in ('string', 'str'):
        return str
    if _str in ('int', 'integer'):
        return int
    if _str in ('float'):
        return float
    raise errors.ParamTypeUnsupported(_str)

    # for future
    if _str in ('list', 'array'):
        return list
    if _str in ('dict', 'dictionary'):
        return dict


def create_parameter(
        name: str,
        param: dict[str, Any]) -> Union[Parameter, GroupParameters]:
    """Function to create a Parameter or GroupParameters instance
    from the name and parameter data.

    Parameters
    ----------
    name : str
        Parameter's name
    param : dict[str, Any]
        Arguments for the parameter initialization

    Returns
    -------
    Union[Parameter, GroupParameters]
        The new Parameter or GroupParameters instance
    """
    if 'type' in param:
        _type = _str_to_pytype(param['type'])

        if _type in (int, float):
            return NumberParameter(name,
                                   _type,
                                   param.get('default'),
                                   param.get('description'),
                                   (param.get('min'), param.get('max')))

        elif _type == str and 'choices' not in param:
            return StringParameter(name,
                                   param.get('default'),
                                   param.get('description'),
                                   (param.get('min'), param.get('max')))

        elif _type == str:
            return ChoiceParameter(name,
                                   param['choices'],
                                   param.get('default'),
                                   param.get('description'))
        return Parameter(name,
                         _type,
                         param.get('default'),
                         param.get('description'))

    else:  # assume that the parameter is a group of parameters
        group = GroupParameters(name, [
            create_parameter(child_name, child_param)
            for child_name, child_param in param.items()
        ])
        return group
