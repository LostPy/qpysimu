from typing import Any


class Config:

    """Base class for configuration class
    """

    def __init__(self, **kwargs):
        """Initialize an instance of Config

        !!! info

            All keywords arguments are set as attribute for this instance.

        Parameters
        ----------
        **kwargs
            Attributes of Config
        """
        for arg_name, arg_value in kwargs.items():
            setattr(self, arg_name, arg_value)

    def __setitem__(self, key: str, value: Any):
        """Set a new attribute or  update an existing attribute.

        Parameters
        ----------
        key : str
            The attribute name
        value : Any
            The attribute value

        Raises
        ------
        ValueError
            If key is not a str
        """
        if not isinstance(key, str):
            raise ValueError(
                f"'key' must be a string, not an instance of '{type(key)}'")
        setattr(self, key, value)

    def __getitem__(self, key: str) -> Any:
        """Get an attribute value.

        Parameters
        ----------
        key : str
            The attribute name

        Returns
        -------
        Any
            The attribute value
        """
        return getattr(self, key)
