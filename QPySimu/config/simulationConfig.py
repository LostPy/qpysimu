from typing import Union, Any
from inspect import ismethod

from .config import Config
from . import create_parameter, Parameter, GroupParameters


class SimuInterfaceConfig(Config):

    """Config class to define the differents parameters of the simulation.

    Attributes
    ----------
    BASE_ATTRIBUTES : tuple[str]
        Base optional settings, None by default
    minimap : bool
        If True, a minimap with the overall view will be displayed.
    zoom : bool
        If True, user can use zoom commands
    parameters : dict[str, Union[Parameter, GroupParameters]]
        Parameters of the simulation
    """

    BASE_ATTRIBUTES = (
        'minimap',
        'zoom',
        'parameters',
    )

    def __init__(self, config: Union[None, dict[str, Any]]):
        """Initialize an instance of SimuInterfaceConfig.

        Parameters
        ----------
        config : Union[None, dict[str, Any]]
            All attributes for the simulation configuration.
        """
        self.minimap = config['minimap'] if 'minimap' in config else False
        self.zoom = config['zoom'] if 'zoom' in config else False
        self.parameters = dict()
        if 'parameters' in config:
            self.parameters = {
                name: create_parameter(name, param)
                for name, param in config['parameters'].items()
            }

        super().__init__(**dict(
            filter(lambda k: k not in self.BASE_ATTRIBUTES, config)
        ))

    def get(self, key: str) -> Union[Parameter, GroupParameters]:
        """Get a parameter with the name

        Parameters
        ----------
        key : str
            Parameter's name

        Returns
        -------
        Union[Parameter, GroupParameters]
            The parameter or group parameters
        """
        return self.parameters.get(key)


class SimulationConfig(Config):

    """Config class to define settings of a simulation.
    This config use real values of parameters, specified by the user in GUI.
    """

    def __init__(self, parameters: dict[str, Union[str, int, float]],
                 groups_parameters: dict[str, dict[Union[str, int, float]]]):
        """Initialize an instance of SimulationConfig

        Parameters
        ----------
        parameters : dict[str, Union[str, int, float]]
            parameters of simulation (not groups), \
            with real values defined by the user.
        groups_parameters : dict[str, dict[Union[str, int, float]]]
            groups parameters of simulation, \
            with real values defined by the user.
        """
        super().__init__(**parameters, **groups_parameters)

    def __setitem__(self, key: str, value: Any):
        """Limit changes to the configuration during the simulation."""
        raise NotImplemented

    def to_dict(self) -> dict:
        """Convert the instance in a dict to save the config simulation.

        Returns
        -------
        dict
            The dict with attributes of the instance.
        """
        result = {
            k: getattr(self, k)
            for k in self.__dir__()
            if not ismethod(getattr(self, k))
            and not k.startswith('_')
        }
        return result

    @classmethod
    def from_dict(cls, d: dict):
        """Create a SimulationConfig instance from a dict
        (created with to_dict method).

        Parameters
        ----------
        d : dict
            The dict created with to_dict method.

        Returns
        -------
        SimulationConfig
            The new SimulationConfig instance initialized with the save
        """
        return cls(d, dict())
