from typing import Any
from yaml import safe_load

from QPySimu.errors import ConfigExeption

from .config import Config
from . import SimuInterfaceConfig


class AppConfig(Config):

    """Subclass of Config to define the application config with:

     * App name
     * Description
     * License
     * Year of release
     * Site url
     * Repository url
     * And other settings

    Attributes
    ----------
    BASE_ATTRIBUTES : tuple[str]
        Base optional settings, None by default
    app_name : str
        The application name (Use for title window)
    """

    BASE_ATTRIBUTES = (
        'description',
        'authors',
        'license',
        'year',
        'site_url',
        'repo_url',
    )

    def __init__(self, app_name: str, **kwargs):
        """Initialize an AppConfig instance

        !!! info

            All keywords arguments are set as attribute for this instance.

        Parameters
        ----------
        app_name : str
            The application name (Use for title window)
        **kwargs
            Other settings
        """
        self.app_name = app_name
        self._simu_config = SimuInterfaceConfig(kwargs.get('simulation'))
        kwargs.pop('simulation')

        for attr_name in self.BASE_ATTRIBUTES:
            setattr(self, attr_name, kwargs.get(attr_name))
            if attr_name in kwargs:
                kwargs.pop(attr_name)

        super().__init__(**kwargs)

    @property
    def app_desc(self) -> str:
        """Alias for description attribute

        Returns
        -------
        str
            The description of application
        """
        return self.description

    @property
    def simu_config(self) -> dict[str, Any]:
        """The simulation interface configs
        A config object with parameters of simulation.

        !!! warning
            It's different of SimulationConfig.

        Returns
        -------
        dict[str, Any]
            Instance of SimuInterfaceConfig
        """
        return self._simu_config

    def get_simu_config(self, key: str) -> Any:
        """Method to access at the simulation config

        Parameters
        ----------
        key : str
            The attribute name of simulation config to get.

        Returns
        -------
        Any
            The value of attribute
        """
        return self._simu_config.get(key)

    @classmethod
    def from_yml(cls, path: str) -> 'AppConfig':
        """Classmethod to create an AppConfig instance from a yaml file.

        !!! info

            See the example of config yaml file

        Parameters
        ----------
        path : str
            The path of yaml file

        Returns
        -------
         AppConfig
            A new instance of AppConfig Initialized with yaml file.

        Raises
        ------
        ConfigExeption
            Exception raised if 'app_name' is not specified
        """
        try:
            with open(path, 'r') as f:
                config = safe_load(f)
            return cls(**config)
        except TypeError:
            raise ConfigExeption(
                "The keyword 'app_name' must be specified in the config file")
