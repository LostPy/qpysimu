from PySide6.QtWidgets import QGraphicsItem
from PySide6.QtCore import QObject, QPointF, Signal, Slot


class ItemDisplayable(QObject):
    """Class to represent an item displayable during the simulation with:
            * Attributes evolving during the simulation
            * An associated QGraphicsItem
    It's recommended to inherit your displayable items from this class.

    ??? example

        ```python
        from PySide6.QtWidgets import QGraphicsRectItem
        from PySide6.QtGui import QColor, QBrush
        from QPySimu.items import ItemDisplayable


        class RedRectItem(ItemDisplayable):

            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.width = 200
                self.height = 150

            def default_graphics_item(self) -> QGraphicsRectItem:
                rect = QGraphicsRectItem(0, 0, self.width, self.height)
                rect.setBrush(QBrush(QColor('red')))
                return rect
        ```

    Attributes
    ----------
    KLASS_VISIBLE : bool
        Class attribute indicating whether objects of this class 
        are visible or not
    visibilityChanged : Signal
        A Qt signal emited when KLASS_VISIBLE changed
    graphics_item : QGraphicsItem
        The QGraphicsItem which represents the item
    """

    KLASS_VISIBLE = True
    ITEMS = dict()

    # Signal emited when KLASS_VISIBLE changed
    visibilityChanged = Signal(bool)

    def __init__(self, init_pos: tuple[float, float] = (0., 0.),
                 is_visible: bool = True, *, _id: int = None):
        """Initialize an instance of ItemDisplayable

        Parameters
        ----------
        init_pos : tuple[float, float], optional
            The initial position of item.
            Default: (0, 0)
        is_visible : bool, optional
            True if this item is displayed in graphic view
            Default: True
        """
        super().__init__()
        if _id in self.ITEMS.keys():
            self._id = int(_id)
        else:
            self._id = self.get_new_id()
            self.item_created(self)
        self._position = QPointF(init_pos[0], init_pos[1])
        self._is_visible = is_visible
        self.graphics_item = self.default_graphics_item()
        self.graphics_item.setPos(self._position)

    @property
    def id(self) -> int:
        """Id of the item.

        Returns
        -------
        int
            The item's id
        """
        return self._id

    @property
    def graphics_item(self) -> QGraphicsItem:
        """The graphics item of this object

        Returns
        -------
        QGraphicsItem
            The graphics item
        """
        return self._graphics_item

    @graphics_item.setter
    def graphics_item(self, new: QGraphicsItem):
        """Change the graphics item

        Parameters
        ----------
        new : QGraphicsItem
            The new graphics item

        Raises
        ------
        ValueError
            Exception raised if the new value is not a QGraphicsItem instance
        """
        if not isinstance(new, QGraphicsItem):
            raise ValueError(
                "'graphics_item' must be an instance of 'QGraphicsItem', "
                f"not an instance of '{type(new)}'"
            )
        self._graphics_item = new

    @property
    def position(self) -> QPointF:
        """The position of the item

        Returns
        -------
        QPointF
            Point to represent the position
        """
        return self._position

    @position.setter
    def position(self, new: QPointF):
        """Change the position of the item

        Parameters
        ----------
        new : QPointF
            Point to represent the position

        Raises
        ------
        ValueError
            Raised if the new value is not a QPointF instance or a tuple.
        """
        if not isinstance(new, (QPointF, tuple, list)):
            raise ValueError(
                "position must be an instance of 'QPointF' "
                "or a tuple of float, "
                f"not an instance of '{type(new)}'")
        if isinstance(new, (tuple, list)):
            try:
                new = QPointF(new[0], new[1])
            except ValueError:
                raise ValueError(
                    "position must be an instance of 'QPointF' "
                    "or a tuple of float, "
                    f"not an instance of '{type(new)}'")
        self.graphics_item.setPos(new)
        self._position = QPointF(new)

    @property
    def x(self) -> float:
        """Returns the x coordinate of item

        Returns
        -------
        float
            The x coordinate
        """
        return self._position.x()

    @x.setter
    def x(self, new: float):
        """Change the x coordinate of item

        Parameters
        ----------
        new : float
            The new value

        Raises
        ------
        ValueError
            Exception raised if the new value is not a float
        """
        if not isinstance(new, (int, float)):
            raise ValueError(
                f"x must be a float, not an instance of '{type(new)}'")
        self._position.setX(new)
        self.graphics_item.setX(new)

    @property
    def y(self) -> float:
        """Returns the y coordinate of item

        Returns
        -------
        float
            The y coordinate
        """
        return self._position.y()

    @y.setter
    def y(self, new: float):
        """Change the y coordinate of item

        Parameters
        ----------
        new : float
            The new value

        Raises
        ------
        ValueError
            Exception raised if the new value is not a float
        """
        if not isinstance(new, (int, float)):
            raise ValueError(
                f"y must be a float, not an instance of '{type(new)}'")
        self._position.setY(new)
        self.graphics_item.setY(new)

    @property
    def is_visible(self) -> bool:
        """Returns if the item is visible or not

        Returns
        -------
        bool
            True if item is visible else False
        """
        return self._is_visible

    @is_visible.setter
    def is_visible(self, new: bool):
        """Change the visibility of item

        Parameters
        ----------
        new : bool
            True to display the item else False
        """
        self._is_visible = bool(new)
        self.graphics_item.setVisible(self._is_visible)

    @Slot(bool)
    def setVisible(self, visible: bool):
        """Slot called when KLASS_VISIBLE changed.
        Update the visibility of graphics_item if self._is_visible is True
        (Priority to self._is_visible attribute).
        """
        if self._is_visible:
            self.graphics_item.setVisible(visible)

    def to_dict(self) -> dict:
        """Returns the item data to save it

        Returns
        -------
        dict
            item data
        """
        return {
            "id": self._id,
            "position": (self.x, self.y),
            "visible": self._is_visible
        }

    @classmethod
    def get_new_id(cls) -> int:
        """Returns a new id

        Returns
        -------
        int
            The new id
        """
        return max(list(cls.ITEMS.keys())) + 1 if len(cls.ITEMS) > 0 else 0

    @classmethod
    def item_created(cls, item):
        """Classmethod called when a item is created.
        Save item references in a dictionary.

        Parameters
        ----------
        item : cls
            The new item
        """
        cls.ITEMS[item.id] = item

    @classmethod
    def clear(cls):
        """Removes all items from dictionary `cls.ITEMS`
        """
        cls.ITEMS = dict()

    @classmethod
    def from_dict(cls, d: dict) -> 'ItemDisplayable':
        """Create a new instance of ItemDisplayable 
        with the data saved by the method to_dict

        Parameters
        ----------
        d : dict
            item data

        Returns
        -------
        ItemDisplayable
            The new instance initialized with item data of the dict
        """
        return cls(
            init_pos=d['position'],
            is_visible=d['visible'],
            _id=d['id']
        )

    @classmethod
    def klass_is_visible(cls) -> bool:
        """Class method, return if the items of this class are displayed or not

        Returns
        -------
        bool
            True if item is visible else False
        """
        return cls.KLASS_VISIBLE

    @classmethod
    def set_klass_visible(cls, visible: bool):
        """Class method to change the visibility of all items of this class

        Parameters
        ----------
        visible : bool
            True to display the items else False
        """
        cls.KLASS_VISIBLE = bool(visible)
        for item in cls.ITEMS.values():
            item.visibilityChanged.emit(cls.KLASS_VISIBLE)
            item.setVisible(cls.KLASS_VISIBLE)

    @classmethod
    def default_graphics_item(cls) -> QGraphicsItem:
        """Classmethod to write in subclass.
        Returns the default QGraphicsItem for this class.

        No Longer Returned
        ------------------
        QGraphicsItem : The default QGraphicsItem instance

        Raises
        ------
        NotImplementedError
            Method to implemented in subclass.
        """
        raise NotImplementedError
