import sys
import os
from pathlib import Path
from argparse import ArgumentParser
import subprocess


NAME = "QPySimu"
DESCRIPTION = """
This CLI allows you to create a new simulation project quickly \
and to access the documentation locally \
as well as to install the documentation dependencies.
"""


def create_parser():
    parser = ArgumentParser(prog=NAME, description=DESCRIPTION)
    parser.add_argument("--new", type=str, default=None, required=False,
                        help="Create a new project at the specified path")
    parser.add_argument("--doc", action='store_true',
                        help="Run a local documentation (with mkdocs)")
    parser.add_argument(
        "--install-doc", action='store_true',
        help="Install the requirements to build the documentation")
    return parser


def _get_pkg_path() -> Path:
    return Path(__file__).resolve().parent


def _get_site_pkg_path() -> Path:
    return _get_pkg_path().parent


def _get_doc_path() -> Path:
    return _get_site_pkg_path() / 'qpysimu_docs'


def install_doc_requirements():
    requirements_path = _get_doc_path() / 'requirements.txt'
    subprocess.check_call(
        [sys.executable, "-m", "pip", "install", "-r", str(requirements_path)])


def run_mkdocs():
    old_path = Path(os.getcwd()).resolve()
    os.chdir(_get_doc_path())
    try:
        subprocess.check_call(['mkdocs', 'serve'])
    except KeyboardInterrupt:
        pass
    finally:
        os.chdir(old_path)


def create_project(path: str):
	print("This feature is not yet avaible.")


if __name__ and '__main__':
    parser = create_parser()
    args = parser.parse_args(sys.argv[1:])

    if args.install_doc:
        install_doc_requirements()

    if args.doc:
        run_mkdocs()

    elif args.new:
    	create_project(args.new)

