from PySide6.QtWidgets import (
    QDialog,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QLineEdit
)
from PySide6.QtCore import QCoreApplication

from QPySimu.config import AppConfig


class AboutDialog(QDialog):

    """A default simple About window.

    This window displays:

      * The application's name
      * The application's description
      * Authors
      * License
      * Repository url
      * Site url
      * Year of release

    !!! info

        Each information is displayed if the value is not None
    """

    tr = QCoreApplication.translate

    def __init__(self, app_config: AppConfig, parent=None):
        """Initialize an about window

        Parameters
        ----------
        app_config : AppConfig
            The application configuration
        parent : QWidget
            The parent widget
        """
        super().__init__(parent)
        main_layout = QVBoxLayout()
        label_desc = QLabel(self.tr("About description title", "Description"))
        self.setWindowTitle(f"{app_config.app_name} - About")
        self.description = QLabel(app_config.app_desc, self)
        main_layout.addWidget(label_desc)
        main_layout.addWidget(self.description)

        labels_layout = QVBoxLayout()
        lineEdit_layout = QVBoxLayout()
        display_layout = QHBoxLayout()

        for attr_name in AppConfig.BASE_ATTRIBUTES:
            if attr_name != "description" and\
                    app_config[attr_name] is not None:
                label = QLabel(self.tr(
                    f"About {attr_name} label", attr_name.capitalize()), self)

                if isinstance(app_config[attr_name], (list, tuple)):
                    line_edit = QLineEdit(
                        "; ".join(list(app_config[attr_name])), self)
                else:
                    line_edit = QLineEdit(str(app_config[attr_name]), self)

                line_edit.setReadOnly(True)
                labels_layout.addWidget(label)
                lineEdit_layout.addWidget(line_edit)

        display_layout.addLayout(labels_layout)
        display_layout.addLayout(lineEdit_layout)
        main_layout.addLayout(display_layout)
        self.setLayout(main_layout)
