from PySide6.QtCore import Slot
from PySide6.QtGui import QPainter

from QPySimu.core import SimuState, SimulationMode

from . import SimulationScene, SimulationView, BaseSimuWindow


class SimuWindow2D(BaseSimuWindow):
    """MainWindow for a simulation in a 2D space
    """

    def __init__(self, *args, **kwargs):
        """Initialize the main window

        Parameters
        ----------
        config_file : str
            The configuration file of application (yaml file)
        items_class : list[type]
            List of item types used in simulation. \
            These types must be subclass of ItemDisplayable.
        parent : None, optional
            The parent QObject
        """
        super().__init__(*args, **kwargs)
        self.simulationScene = SimulationScene(self)
        self.simulationView = SimulationView(self.simulationScene, self)
        self.init_view()
        self.viewLayout.insertWidget(0, self.simulationView)

    def init_view(self):
        """Initialize the graphics view.
        """
        self.simulationView.setInteractive(False)
        self.simulationView.setRenderHints(QPainter.Antialiasing)
        if self.config.simu_config.zoom is not None:
            self.simulationView.ZOOM_ENABLE = self.config.simu_config.zoom

    def connections(self):
        """Connect some slot to signal
        """
        super().connections()
        self.simu_result.started.connect(self.on_simu_result_started)

    @Slot()
    def on_simu_result_started(self):
        """Slot called when `self.sumu_result` started
        Reset the scene (clear all items of the scene).
        """
        self.simu_result.setMode(SimulationMode.PAUSE)
        self.simulationScene.current_state = None
        for type_ in self.filter_widgets.keys():
            type_.clear()
        self.simulationScene.clear()
        self.simu_result.setMode(
            SimulationMode.STEP_BY_STEP if self.checkBoxStepByStep.isChecked()
            else SimulationMode.RUN
        )

    @Slot(float, int, float, SimuState)
    def on_simu_result_progressed(self,
                                  t: float,
                                  time: int,
                                  progress: float,
                                  state: SimuState):
        """Slot called during the progression of simulation result.
        Manage the view and data displayed.

        Parameters
        ----------
        t : float
            The current time of the simulation
        time : int
            The duration of the simulation
        progress : float
            The progression (number between 0 and 1)
        state : SimuState
            The current state
        """
        super().on_simu_result_progressed(t, time, progress, state)
        self.simulationScene.newState(state)
