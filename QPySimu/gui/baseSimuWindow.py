from PySide6.QtWidgets import QMainWindow, QSpacerItem, QSizePolicy
from PySide6.QtCore import Qt, Slot
from PySide6.QtGui import QIcon, QCloseEvent

from QPySimu.core import (
    Simulation,
    SimuState,
    SimulationResult,
    SimulationMode
)
from QPySimu.config import (
    AppConfig,
    SimulationConfig,
    Parameter,
    GroupParameters
)
from QPySimu.items import ItemDisplayable
from QPySimu.ressources import icons_rc
from QPySimu.utils import iosaves

from .ui import Ui_QPySimuWindow
from .widgets import (
    create_parameter_widget,
    GroupParametersWidget,
    ItemFilterWidget
)
from . import AboutDialog


class BaseSimuWindow(Ui_QPySimuWindow, QMainWindow):

    """Base class for main window.
    This window contains:
      * The initialized parameters area
      * The item filter
      * The widgets used for the calculation of the simulation
      * The widgets used for reading the results.

    !!! warning

        This window does not manage the graphical views

    Attributes
    ----------
    simu_result : SimulationResult
        The result of the simulation
    config : AppConfig
        The configuration of application
    parameters_widgets : dict[str, ParameterWidget]
        A dictionary with the parameters widgets that are not in a group.
        Key = parameter name & Value = ParameterWidget
    group_parameters_widgets : dict[str, GroupParametersWidget]
         A dictionary with the group of parameters widgets
         Key = group name & Value = GroupParameters
    filter_widgets : dict[type, ItemFilterWidget]
        Dictionary with filter widgets in values and \
        the name of the type in keys.
    simulation : Simulation
        The instance of Simulation that contains the algorithm
    """

    def __init__(self,
                 config_file: str,
                 items_class: list[type] = [],
                 parent=None):
        """Initialize the main window

        Parameters
        ----------
        config_file : str
            The configuration file of application (yaml file)
        items_class : list[type]
            List of item types used in simulation. \
            These types must be subclass of ItemDisplayable.
        parent : None, optional
            The parent QObject
        """
        super().__init__(parent)
        self.setupUi(self)
        self.init_icons()
        self.init_app(config_file)
        self.init_parameters()
        self.init_filter(items_class)
        self.simulation: Simulation = None
        self.simu_result = SimulationResult()
        self.sliderFrequency.setValue(self.simu_result.frequency)
        self.labelFrequency.setText(str(self.simu_result.frequency))
        self.connections()

    # -------------------------------
    # Initialization
    # -------------------------------
    def init_icons(self):
        """Set icons in widgets
        """
        self.buttonPlay.setIcon(QIcon(":/playback/play"))
        self.buttonStop.setIcon(QIcon(":/playback/stop"))
        self.buttonNext.setIcon(QIcon(":/playback/skip-forward"))
        self.buttonPrevious.setIcon(QIcon(":/playback/skip-back"))

    def init_app(self, config_file: str):
        """Initialize the application (config, title...)

        Parameters
        ----------
        config_file : str
            The config file
        """
        self.config = AppConfig.from_yml(config_file)
        self.setWindowTitle(self.config.app_name)

    def init_parameters(self):
        """Initialize parameters widgets
        """
        self.parameters_widgets = dict()
        self.group_parameters_widgets = dict()

        for param in self.config.simu_config.parameters.values():

            if isinstance(param, Parameter):
                widget = create_parameter_widget(param, self)
                self.parameters_widgets[param.name] = widget
                self.layoutParam.insertWidget(
                    len(self.parameters_widgets) - 1, widget)

            elif isinstance(param, GroupParameters):
                widget = GroupParametersWidget(param, self)
                self.group_parameters_widgets[param.name] = widget
                self.layoutParam.addWidget(widget)
        self.layoutParam.addSpacerItem(QSpacerItem(
            20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))

    def init_filter(self, items_class: list[type]):
        """Initialize items filter

        Parameters
        ----------
        items_class : list[type]
            List of item types used in simulation. \
            These types must be subclass of ItemDisplayable.
        """
        self.filter_widgets = dict()

        for type_ in items_class:

            if issubclass(type_, ItemDisplayable):
                widget = ItemFilterWidget(type_, self)
                self.filter_widgets[type_] = widget
                self.layoutFilter.addWidget(widget)
        self.layoutFilter.addSpacerItem(QSpacerItem(
            20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding))
        iosaves.add_item_types(items_class)

    # -------------------------------
    # Methods
    # -------------------------------
    def setStopEnable(self, enable: bool):
        """Enable or disable stop button and stop action

        Parameters
        ----------
        enable : bool
            True to enable widgets else False
        """
        self.buttonStop.setEnabled(enable)
        self.actionStop.setEnabled(enable)

    def setStepEnable(self, enable: bool):
        """Enable or disable next and previous buttons and actions

        Parameters
        ----------
        enable : bool
            True to enable widgets else False
        """
        self.buttonNext.setEnabled(enable)
        self.buttonPrevious.setEnabled(enable)
        self.actionNext.setEnabled(enable)
        self.actionPrevious.setEnabled(enable)

    def run_to_stepbystep(self):
        """Method to set the STEP_BY_STEP mode from the RUN mode.
        """
        if self.simu_result.mode == SimulationMode.RUN\
                or self.simu_result.mode == SimulationMode.PAUSE:
            self.checkBoxStepByStep.setChecked(True)

    def setSimulation(self, simulation: Simulation):
        """Set a new simulation.

        Parameters
        ----------
        simulation : Simulation
            The simulation to set
        """
        self.simulation = simulation
        self.simu_result.save_file = self.simulation.save_file
        self.simulation.progressed.connect(self.on_simulation_progressed)
        self.simulation.finished.connect(self.on_simulation_finished)

    # -------------------------------
    # Slots
    # -------------------------------
    def connections(self):
        """Connect some slot to signal
        """
        self.simu_result.finished.connect(self.on_simu_result_finished)
        self.simu_result.progressed.connect(self.on_simu_result_progressed)
        self.simu_result.modeChanged.connect(self.on_simu_result_modeChanged)

    @Slot(SimulationMode)
    def on_simu_result_modeChanged(self, mode: SimulationMode):
        """Slot called when `self.sumu_result.mode` changed.
        Update the buttonPlay's icon.

        Parameters
        ----------
        mode : SimulationMode
            The new mode of `self.simu_result`
        """
        if mode == SimulationMode.RUN:
            self.buttonPlay.setIcon(QIcon(":/playback/pause"))
        else:
            self.buttonPlay.setIcon(QIcon(":/playback/play"))

    @Slot()
    def on_simu_result_finished(self):
        """Slot called when the simulation result is finished.
        """
        self.on_actionStop_triggered()

    @Slot(float, int, float, SimuState)
    def on_simu_result_progressed(self,
                                  t: float,
                                  time: int,
                                  progress: float,
                                  state: SimuState):
        """Slot called during the progression of simulation result.
        Manage the view and data displayed.

        Parameters
        ----------
        t : float
            The current time of the simulation
        time : int
            The duration of the simulation
        progress : float
            The progression (number between 0 and 1)
        state : SimuState
            The current state
        """
        self.labelTime.setText(f"t={t}/{time}")
        self.calculProgress.setValue(int(progress * 100))

    @Slot()
    def on_simulation_finished(self):
        """Slot called when the simulation calculation is finished.

        Re-enables all widgets that have been disabled.
        """
        self.tabWidgetTools.setEnabled(True)
        self.buttonPlay.setEnabled(True)
        self.actionPlay.setEnabled(True)
        self.checkBoxStepByStep.setEnabled(True)
        self.sliderFrequency.setEnabled(True)
        self.labelFrequency.setEnabled(True)
        self.buttonCalcul.setEnabled(True)
        self.actionCalculate.setEnabled(True)

    @Slot(float, int)
    def on_simulation_progressed(self, t: float, progress: float):
        """Slot called during the progression of simulation calculation.

        Parameters
        ----------
        t : float
            The current time of the simulation
        progress : float
            The progression (number between 0 and 1)
        """
        self.labelTime.setText(f"t={t}")
        self.calculProgress.setValue(int(progress * 100))

    @Slot()
    def on_buttonHideParam_clicked(self):
        """Slot called when the buttonHideParam is clicked.
        Hide the paramers and filter sections
        """
        self.tabWidgetTools.setVisible(self.tabWidgetTools.isHidden())
        self.buttonHideParam.setArrowType(
            Qt.RightArrow if self.tabWidgetTools.isHidden() else Qt.LeftArrow)

    @Slot(int)
    def on_checkBoxStepByStep_stateChanged(self, state: int):
        """Slot called when the state of checkbox STEP_BY_STEP changed.
        If checked, the simulation result mode is STEP_BY_STEP else RUN \
        (if not STOP or PAUSE).

        This slot updates the icon of play button and \
        the mode of simulation result.

        Parameters
        ----------
        state : int
            The state of checkbox
        """
        if self.simu_result.mode != SimulationMode.STOP and bool(state):
            self.simu_result.setMode(SimulationMode.STEP_BY_STEP)

        elif self.simu_result != SimulationMode.STOP:
            self.simu_result.setMode(SimulationMode.RUN)

    @Slot(int)
    def on_sliderFrequency_valueChanged(self, value: int):
        """Slot called when the value of sliderFrequency changed.
        This slot updates the labelFrequency content with the new value,
        and updates the value of simu_result.frequency

        Parameters
        ----------
        value : int
            The new value
        """
        self.labelFrequency.setText(str(value))
        self.simu_result.frequency = value

    @Slot()
    def on_actionCalculate_triggered(self):
        """Slot called when the actionCalculate is triggered.
        This slot disable a lot of widgets and start the simulation \
        with current parameters.
        """
        self.on_actionStop_triggered()
        self.tabWidgetTools.setEnabled(False)
        self.buttonPlay.setEnabled(False)
        self.actionPlay.setEnabled(False)
        self.checkBoxStepByStep.setEnabled(False)
        self.buttonCalcul.setEnabled(False)
        self.actionCalculate.setEnabled(False)
        self.sliderFrequency.setEnabled(False)
        self.labelFrequency.setEnabled(False)
        self.calculProgress.setValue(0)
        self.simulation.start(SimulationConfig(
            parameters={
                name: widget.get_value()
                for name, widget in self.parameters_widgets.items()
            },
            groups_parameters={
                name: widget.to_dict()
                for name, widget in self.group_parameters_widgets.items()
            }
        ))

    @Slot()
    def on_actionPlay_triggered(self):
        """Slot called when the actionPlay is triggered.
        This slot updates the mode of simu_result, \
        the icon of play button and the state of checkBoxStepByStep.
        """
        if not self.simu_result.isRunning():
            if self.checkBoxStepByStep.isChecked():
                self.simu_result.setMode(SimulationMode.STEP_BY_STEP)
            else:
                self.simu_result.setMode(SimulationMode.RUN)
            self.simu_result.start()
            self.setStepEnable(True)
            self.setStopEnable(True)

        elif self.simu_result.mode == SimulationMode.STEP_BY_STEP:
            self.simu_result.setMode(SimulationMode.RUN)
            self.checkBoxStepByStep.setChecked(False)

        elif self.simu_result.mode == SimulationMode.PAUSE:
            self.simu_result.setMode(SimulationMode.RUN)

        elif self.simu_result.mode == SimulationMode.RUN:
            self.simu_result.setMode(SimulationMode.PAUSE)

    @Slot()
    def on_actionStop_triggered(self):
        """Slot called when the actionStop is triggered.

        Disable stop, next and previous buttons, \
        updates the mode and icons of play button.
        """
        self.setStopEnable(False)
        self.setStepEnable(False)
        self.simu_result.setMode(SimulationMode.STOP)

    @Slot()
    def on_actionNext_triggered(self):
        """Slot called when the actionNext is triggered.
        Switches to the next state
        """
        self.run_to_stepbystep()
        self.simu_result.next()

    @Slot()
    def on_actionPrevious_triggered(self):
        """Slot called when the actionPrevious is triggered.
        Switches to the previous state
        """
        self.run_to_stepbystep()
        self.simu_result.previous()

    @Slot()
    def on_actionAbout_triggered(self):
        """Slot called when the actionAbout is triggered.
        Open the about window
        """
        about = AboutDialog(self.config, self)
        about.exec()

    @Slot()
    def on_actionQuit_triggered(self):
        """Slot called when the actionQuit is triggered.
        Close the window
        """
        self.close()

    # -------------------------------
    # Events
    # -------------------------------
    def closeEvent(self, event: QCloseEvent):
        """Method called when a closeEvent is triggered.
        Accepts the event.

        Parameters
        ----------
        event : QCloseEvent
            The clase event
        """
        event.accept()
