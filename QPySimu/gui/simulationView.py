from PySide6.QtWidgets import QGraphicsView, QGraphicsScene
from PySide6.QtCore import QPointF, Qt
from PySide6.QtGui import QWheelEvent, QMouseEvent


class SimulationView(QGraphicsView):

    """Subclass of QGraphicsView to define the main view of GUI.

    Attributes
    ----------
    ZOOM_SENSIBILITY : float
        The zoom sensibility. Must be between 0 and 1.
    ZOOM_ENABLE : bool
        If True, enable the zoom feature.
    """

    ZOOM_SENSIBILITY = 0.3
    ZOOM_ENABLE = True

    def __init__(self, scene: QGraphicsScene, parent=None):
        """Initialize the view instance

        Parameters
        ----------
        scene : QGraphicsScene
            The scene to display
        parent : None, optional
            The parent widget
        """
        super().__init__(scene, parent)
        self.setDragMode(QGraphicsView.ScrollHandDrag)

    def zoom(self, factor: float):
        """Zoom on the view

        Parameters
        ----------
        factor : float
            The zoom factor
        """
        if factor < 0:
            factor = -1 / factor
        self.scale(factor, factor)

    def wheelEvent(self, event: QWheelEvent):
        """Wheel Event: \
        manages the zoom if the angle of the wheel has been changed

        Parameters
        ----------
        event : QWheelEvent
            The event raised
        """
        if self.ZOOM_ENABLE:
            f = event.angleDelta().y() / (
                100 * (1.18 * (1 - self.ZOOM_SENSIBILITY) + 0.01))
            self.zoom(f)
        else:
            super().wheelEvent(event)
