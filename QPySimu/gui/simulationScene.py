from PySide6.QtWidgets import QGraphicsScene

from QPySimu.core import SimuState


class SimulationScene(QGraphicsScene):

    """Subclass of QGraphicsScene to define the simulation scene
    """

    def __init__(self, parent=None):
        """Initialize the new SimulationScene instance.

        Parameters
        ----------
        parent : None, optional
            The parent widget
        """
        super().__init__(parent)
        self.current_state: SimuState = None

    def newState(self, state: SimuState):
        """Update the state with all items position in the scene.

        Parameters
        ----------
        state : SimuState
            The new state
        """
        if self.current_state is None:
            self.current_state = state
            for item in self.current_state.displayable_items.values():
                self.addItem(item.graphics_item)

        else:
            displayable_items = self.current_state.displayable_items

            for id_, item in state.displayable_items.items():
                if id_ in displayable_items.keys():
                    displayable_items[id_].position = item.position
                else:
                    self.addItem(item.graphics_item)
                    displayable_items[id_] = item
