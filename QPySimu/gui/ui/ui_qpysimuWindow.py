# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'qpysimuWindow.ui'
##
## Created by: Qt User Interface Compiler version 6.2.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QFrame, QHBoxLayout,
    QLabel, QMainWindow, QMenu, QMenuBar,
    QProgressBar, QPushButton, QScrollArea, QSizePolicy,
    QSlider, QSpacerItem, QStatusBar, QTabWidget,
    QToolButton, QVBoxLayout, QWidget)

class Ui_QPySimuWindow(object):
    def setupUi(self, QPySimuWindow):
        if not QPySimuWindow.objectName():
            QPySimuWindow.setObjectName(u"QPySimuWindow")
        QPySimuWindow.resize(1121, 600)
        self.actionAbout = QAction(QPySimuWindow)
        self.actionAbout.setObjectName(u"actionAbout")
        self.actionPlay = QAction(QPySimuWindow)
        self.actionPlay.setObjectName(u"actionPlay")
        self.actionStop = QAction(QPySimuWindow)
        self.actionStop.setObjectName(u"actionStop")
        self.actionStop.setEnabled(False)
        self.actionNext = QAction(QPySimuWindow)
        self.actionNext.setObjectName(u"actionNext")
        self.actionPrevious = QAction(QPySimuWindow)
        self.actionPrevious.setObjectName(u"actionPrevious")
        self.actionQuit = QAction(QPySimuWindow)
        self.actionQuit.setObjectName(u"actionQuit")
#if QT_CONFIG(shortcut)
        self.actionQuit.setShortcut(u"Ctrl+Q")
#endif // QT_CONFIG(shortcut)
        self.actionCalculate = QAction(QPySimuWindow)
        self.actionCalculate.setObjectName(u"actionCalculate")
        self.centralwidget = QWidget(QPySimuWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.horizontalLayout_2 = QHBoxLayout(self.centralwidget)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.tabWidgetTools = QTabWidget(self.centralwidget)
        self.tabWidgetTools.setObjectName(u"tabWidgetTools")
        self.tabWidgetTools.setMaximumSize(QSize(400, 16777215))
        self.tab_param = QWidget()
        self.tab_param.setObjectName(u"tab_param")
        self.verticalLayout = QVBoxLayout(self.tab_param)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.scrollAreaParam = QScrollArea(self.tab_param)
        self.scrollAreaParam.setObjectName(u"scrollAreaParam")
        self.scrollAreaParam.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 167, 487))
        self.verticalLayout_2 = QVBoxLayout(self.scrollAreaWidgetContents)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.layoutParam = QVBoxLayout()
        self.layoutParam.setObjectName(u"layoutParam")

        self.verticalLayout_2.addLayout(self.layoutParam)

        self.scrollAreaParam.setWidget(self.scrollAreaWidgetContents)

        self.verticalLayout.addWidget(self.scrollAreaParam)

        self.tabWidgetTools.addTab(self.tab_param, "")
        self.tab__filter = QWidget()
        self.tab__filter.setObjectName(u"tab__filter")
        self.verticalLayout_5 = QVBoxLayout(self.tab__filter)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.scrollAreaFilter = QScrollArea(self.tab__filter)
        self.scrollAreaFilter.setObjectName(u"scrollAreaFilter")
        self.scrollAreaFilter.setWidgetResizable(True)
        self.scrollAreaWidgetContents_2 = QWidget()
        self.scrollAreaWidgetContents_2.setObjectName(u"scrollAreaWidgetContents_2")
        self.scrollAreaWidgetContents_2.setGeometry(QRect(0, 0, 167, 487))
        self.verticalLayout_4 = QVBoxLayout(self.scrollAreaWidgetContents_2)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.layoutFilter = QVBoxLayout()
        self.layoutFilter.setObjectName(u"layoutFilter")

        self.verticalLayout_4.addLayout(self.layoutFilter)

        self.scrollAreaFilter.setWidget(self.scrollAreaWidgetContents_2)

        self.verticalLayout_5.addWidget(self.scrollAreaFilter)

        self.tabWidgetTools.addTab(self.tab__filter, "")

        self.horizontalLayout_2.addWidget(self.tabWidgetTools)

        self.buttonHideParam = QToolButton(self.centralwidget)
        self.buttonHideParam.setObjectName(u"buttonHideParam")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonHideParam.sizePolicy().hasHeightForWidth())
        self.buttonHideParam.setSizePolicy(sizePolicy)
        self.buttonHideParam.setMinimumSize(QSize(0, 0))
        self.buttonHideParam.setText(u"")
        self.buttonHideParam.setPopupMode(QToolButton.DelayedPopup)
        self.buttonHideParam.setAutoRaise(True)
        self.buttonHideParam.setArrowType(Qt.LeftArrow)

        self.horizontalLayout_2.addWidget(self.buttonHideParam)

        self.viewLayout = QVBoxLayout()
        self.viewLayout.setObjectName(u"viewLayout")
        self.frameCommands = QFrame(self.centralwidget)
        self.frameCommands.setObjectName(u"frameCommands")
        self.frameCommands.setFrameShape(QFrame.StyledPanel)
        self.frameCommands.setFrameShadow(QFrame.Raised)
        self.verticalLayout_3 = QVBoxLayout(self.frameCommands)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(200, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.checkBoxStepByStep = QCheckBox(self.frameCommands)
        self.checkBoxStepByStep.setObjectName(u"checkBoxStepByStep")

        self.horizontalLayout.addWidget(self.checkBoxStepByStep)

        self.buttonPrevious = QPushButton(self.frameCommands)
        self.buttonPrevious.setObjectName(u"buttonPrevious")
        self.buttonPrevious.setText(u"")

        self.horizontalLayout.addWidget(self.buttonPrevious)

        self.buttonPlay = QPushButton(self.frameCommands)
        self.buttonPlay.setObjectName(u"buttonPlay")
        self.buttonPlay.setText(u"")

        self.horizontalLayout.addWidget(self.buttonPlay)

        self.buttonStop = QPushButton(self.frameCommands)
        self.buttonStop.setObjectName(u"buttonStop")
        self.buttonStop.setEnabled(False)

        self.horizontalLayout.addWidget(self.buttonStop)

        self.buttonNext = QPushButton(self.frameCommands)
        self.buttonNext.setObjectName(u"buttonNext")

        self.horizontalLayout.addWidget(self.buttonNext)

        self.sliderFrequency = QSlider(self.frameCommands)
        self.sliderFrequency.setObjectName(u"sliderFrequency")
        self.sliderFrequency.setMaximumSize(QSize(120, 16777215))
        self.sliderFrequency.setMinimum(1)
        self.sliderFrequency.setMaximum(60)
        self.sliderFrequency.setValue(30)
        self.sliderFrequency.setOrientation(Qt.Horizontal)

        self.horizontalLayout.addWidget(self.sliderFrequency)

        self.labelFrequency = QLabel(self.frameCommands)
        self.labelFrequency.setObjectName(u"labelFrequency")

        self.horizontalLayout.addWidget(self.labelFrequency)

        self.horizontalSpacer_2 = QSpacerItem(200, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)


        self.verticalLayout_3.addLayout(self.horizontalLayout)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.buttonCalcul = QPushButton(self.frameCommands)
        self.buttonCalcul.setObjectName(u"buttonCalcul")

        self.horizontalLayout_3.addWidget(self.buttonCalcul)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_3)

        self.labelTime = QLabel(self.frameCommands)
        self.labelTime.setObjectName(u"labelTime")
        self.labelTime.setText(u"t=0")

        self.horizontalLayout_3.addWidget(self.labelTime)

        self.calculProgress = QProgressBar(self.frameCommands)
        self.calculProgress.setObjectName(u"calculProgress")
        self.calculProgress.setMaximumSize(QSize(200, 16777215))
        self.calculProgress.setValue(0)

        self.horizontalLayout_3.addWidget(self.calculProgress)


        self.verticalLayout_3.addLayout(self.horizontalLayout_3)


        self.viewLayout.addWidget(self.frameCommands)


        self.horizontalLayout_2.addLayout(self.viewLayout)

        QPySimuWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(QPySimuWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1121, 22))
        self.menu_File = QMenu(self.menubar)
        self.menu_File.setObjectName(u"menu_File")
        self.menu_Simulation = QMenu(self.menubar)
        self.menu_Simulation.setObjectName(u"menu_Simulation")
        self.menu_Help = QMenu(self.menubar)
        self.menu_Help.setObjectName(u"menu_Help")
        QPySimuWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(QPySimuWindow)
        self.statusbar.setObjectName(u"statusbar")
        QPySimuWindow.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menu_File.menuAction())
        self.menubar.addAction(self.menu_Simulation.menuAction())
        self.menubar.addAction(self.menu_Help.menuAction())
        self.menu_File.addAction(self.actionQuit)
        self.menu_Simulation.addAction(self.actionCalculate)
        self.menu_Simulation.addSeparator()
        self.menu_Simulation.addAction(self.actionPlay)
        self.menu_Simulation.addAction(self.actionStop)
        self.menu_Simulation.addAction(self.actionNext)
        self.menu_Simulation.addAction(self.actionPrevious)
        self.menu_Simulation.addSeparator()
        self.menu_Help.addAction(self.actionAbout)

        self.retranslateUi(QPySimuWindow)
        self.buttonPlay.clicked.connect(self.actionPlay.trigger)
        self.buttonStop.clicked.connect(self.actionStop.trigger)
        self.buttonPrevious.clicked.connect(self.actionPrevious.trigger)
        self.buttonNext.clicked.connect(self.actionNext.trigger)
        self.actionQuit.triggered.connect(QPySimuWindow.close)
        self.buttonCalcul.clicked.connect(self.actionCalculate.trigger)

        self.tabWidgetTools.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(QPySimuWindow)
    # setupUi

    def retranslateUi(self, QPySimuWindow):
        QPySimuWindow.setWindowTitle(QCoreApplication.translate("QPySimuWindow", u"QPySimuWindow", None))
        self.actionAbout.setText(QCoreApplication.translate("QPySimuWindow", u"About", None))
        self.actionPlay.setText(QCoreApplication.translate("QPySimuWindow", u"&Play/&Pause", None))
#if QT_CONFIG(shortcut)
        self.actionPlay.setShortcut(QCoreApplication.translate("QPySimuWindow", u"F6", None))
#endif // QT_CONFIG(shortcut)
        self.actionStop.setText(QCoreApplication.translate("QPySimuWindow", u"&Stop", None))
#if QT_CONFIG(shortcut)
        self.actionStop.setShortcut(QCoreApplication.translate("QPySimuWindow", u"F7", None))
#endif // QT_CONFIG(shortcut)
        self.actionNext.setText(QCoreApplication.translate("QPySimuWindow", u"&Next", None))
#if QT_CONFIG(shortcut)
        self.actionNext.setShortcut(QCoreApplication.translate("QPySimuWindow", u"F9", None))
#endif // QT_CONFIG(shortcut)
        self.actionPrevious.setText(QCoreApplication.translate("QPySimuWindow", u"&Previous", None))
#if QT_CONFIG(shortcut)
        self.actionPrevious.setShortcut(QCoreApplication.translate("QPySimuWindow", u"F8", None))
#endif // QT_CONFIG(shortcut)
        self.actionQuit.setText(QCoreApplication.translate("QPySimuWindow", u"&Quit", None))
        self.actionCalculate.setText(QCoreApplication.translate("QPySimuWindow", u"Calculate", None))
#if QT_CONFIG(tooltip)
        self.actionCalculate.setToolTip(QCoreApplication.translate("QPySimuWindow", u"Run simulation", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionCalculate.setShortcut(QCoreApplication.translate("QPySimuWindow", u"F5", None))
#endif // QT_CONFIG(shortcut)
        self.tabWidgetTools.setTabText(self.tabWidgetTools.indexOf(self.tab_param), QCoreApplication.translate("QPySimuWindow", u"Parameters", None))
        self.tabWidgetTools.setTabText(self.tabWidgetTools.indexOf(self.tab__filter), QCoreApplication.translate("QPySimuWindow", u"Filter", None))
        self.checkBoxStepByStep.setText(QCoreApplication.translate("QPySimuWindow", u"Step by Step", None))
        self.buttonStop.setText("")
        self.buttonNext.setText("")
#if QT_CONFIG(tooltip)
        self.sliderFrequency.setToolTip(QCoreApplication.translate("QPySimuWindow", u"Number of iteration /s", None))
#endif // QT_CONFIG(tooltip)
        self.labelFrequency.setText(QCoreApplication.translate("QPySimuWindow", u"30", None))
        self.buttonCalcul.setText(QCoreApplication.translate("QPySimuWindow", u"Calculate", None))
        self.menu_File.setTitle(QCoreApplication.translate("QPySimuWindow", u"&File", None))
        self.menu_Simulation.setTitle(QCoreApplication.translate("QPySimuWindow", u"&Simulation", None))
        self.menu_Help.setTitle(QCoreApplication.translate("QPySimuWindow", u"&Help", None))
    # retranslateUi

