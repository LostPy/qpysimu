from PySide6.QtWidgets import QCheckBox

from QPySimu.items import ItemDisplayable


class ItemFilterWidget(QCheckBox):

    """Subclass of QCheckBox to filter a type of item.

    Attributes
    ----------
    item_type : type
        The type of item, must be a subclass of ItemDisplayable
    """

    def __init__(self, item_type: type, parent=None):
        """Initialize the widget.

        Parameters
        ----------
        item_type : type
            The type of item, must be a subclass of ItemDisplayable.
        parent : None, optional
            The parent widget

        Raises
        ------
        ValueError
            Raised if `item_type` is not a subclass of ItemDisplayable.
        """
        if not issubclass(item_type, ItemDisplayable):
            raise ValueError(
                "'item_type' must be a subclass of ItemDisplayable")
        super().__init__(item_type.__name__, parent)
        self.item_type = item_type
        self.setChecked(self.item_type.klass_is_visible())
        self.stateChanged.connect(self.on_stateChanged)

    def on_stateChanged(self, state: int):
        """Update the visibility of the item type

        Parameters
        ----------
        state : int
            The state of checkbox
        """
        self.item_type.set_klass_visible(bool(state))
