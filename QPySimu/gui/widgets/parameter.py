from typing import Union, Any
from PySide6.QtWidgets import (
    QWidget,
    QGroupBox,
    QLabel,
    QSpinBox,
    QDoubleSpinBox,
    QLineEdit,
    QComboBox,
    QVBoxLayout,
    QHBoxLayout,
)


from QPySimu.config import (
    Parameter,
    GroupParameters,
    NumberParameter,
    StringParameter,
    ChoiceParameter,
)


class ParameterWidget(QWidget):

    """Base class for parameter widgets.
    A widget to allow the modification of a parameter value by the user.

    !!! warning

        This class should not be used, you can directly use 
        the derived class corresponding to the parameter type.

    Attributes
    ----------
    parameter : Parameter
        The parameter
    nameLabel : QLabel
        The label displaying the name of the parameter
    valueWidget : QWidget
        The input widget to change the value
    """

    def __init__(self,
                 parameter: Parameter,
                 widget_klass: type,
                 parent=None):
        """Initialize a ParameterWidget

        Parameters
        ----------
        parameter : Parameter
            The parameter managed by this widget
        widget_klass : type
            The class of input widget (to change the value)
        parent : QWidget, optional
            The widget parent
        """
        super().__init__(parent)
        self.parameter = parameter
        main_layout = QHBoxLayout()
        self.nameLabel = QLabel(self.parameter.name)
        self.valueWidget = widget_klass(self)
        if self.parameter.description:
            self.nameLabel.setToolTip(self.parameter.description)
            self.valueWidget.setToolTip(self.parameter.description)
        main_layout.addWidget(self.nameLabel)
        main_layout.addWidget(self.valueWidget)
        self.setLayout(main_layout)

    def get_value(self):
        """Returns the value of input widget (valueWidget).
        Method to implemente in subclass.

        Raises
        ------
        NotImplemented
            Method to implemente in subclass
        """
        raise NotImplemented


class NumberParameterWidget(ParameterWidget):

    """Class for a parameter of type 'number' (float or int)

    Attributes
    ----------
    DEFAULT_MINIMUM : int
        The default minimum value of the input widget
        Default: -999 999
    DEFAULT_MAXIMUM : int
        The default maximum value of the input widget
        Default: 999 999
    """

    DEFAULT_MINIMUM = -999_999
    DEFAULT_MAXIMUM = 999_999

    def __init__(self, parameter: NumberParameter, parent=None):
        """Initialize the NumberParameterWidget

        Parameters
        ----------
        parameter : NumberParameter
            The parameter managed by this widget
        parent : QWidget, optional
            The widget parent
        """
        if parameter.param_type == float:
            widget_klass = QDoubleSpinBox
        else:
            widget_klass = QSpinBox

        super().__init__(parameter, widget_klass, parent)
        self.valueWidget.setMinimum(
            self.parameter.min if self.parameter.min else self.DEFAULT_MINIMUM)
        self.valueWidget.setMaximum(
            self.parameter.max if self.parameter.max else self.DEFAULT_MAXIMUM)
        self.valueWidget.setValue(
            self.parameter.default if self.parameter.default
            else self.parameter.param_type(
                (self.DEFAULT_MAXIMUM + self.DEFAULT_MINIMUM) / 2)
        )

    def get_value(self) -> Union[int, float]:
        """Returns the value of input widget (valueWidget).

        Returns
        -------
        Union[int, float]
            The current value of parameter
        """
        return self.valueWidget.value()


class StringParameterWidget(ParameterWidget):

    """Class for a parameter of type 'string' (str).
    """

    AUTO_STRIP = True

    def __init__(self, parameter: StringParameter, parent=None):
        """Initialize the StringParameterWidget

        Parameters
        ----------
        parameter : StringParameter
            The parameter managed by this widget
        parent : QWidget, optional
            The widget parent
        """
        super().__init__(parameter, QLineEdit, parent)
        self.valueWidget.setText(
            self.parameter.default if self.parameter.default else "")

    def get_value(self) -> str:
        """Returns the value of input widget (valueWidget).

        Returns
        -------
        str
            The current value of parameter
        """
        if self.AUTO_STRIP:
            return self.valueWidget.text().strip()
        return self.valueWidget.text()


class ChoiceParameterWidget(ParameterWidget):

    """Class for a parameter of type 'string' (str) with a list of choices.
    """

    def __init__(self, parameter: ChoiceParameter, parent=None):
        """Initialize the ChoiceParameterWidget

        Parameters
        ----------
        parameter : ChoiceParameter
            The parameter managed by this widget
        parent : QWidget, optional
            The widget parent
        """
        super().__init__(parameter, QComboBox, parent)
        self.valueWidget.addItems(self.parameter.choices)
        if self.parameter.default:
            self.valueWidget.setCurrentText(self.parameter.default)

    def get_value(self) -> str:
        """Returns the value of input widget (valueWidget).

        Returns
        -------
        str
            The current value of parameter
        """
        return self.valueWidget.currentText()


def create_parameter_widget(
        parameter: Parameter, parent=None) -> ParameterWidget:
    """Create a ParameterWidget from a parameter (Parameter instance)

    Parameters
    ----------
    parameter : Parameter
        The parameter for the new widget
    parent : None, optional
        The parent widget

    Returns
    -------
    ParameterWidget
        The new ParameterWidget object

    Raises
    ------
    ValueError
        Exception raised if the type of parameter is not supported
    """
    if isinstance(parameter, NumberParameter):
        return NumberParameterWidget(parameter, parent)

    elif isinstance(parameter, ChoiceParameter):
        return ChoiceParameterWidget(parameter, parent)

    elif isinstance(parameter, StringParameter):
        return StringParameter(parameter, parent)

    raise ValueError(
        f"Type of parameter not supported: '{type(parameter)}'"
        "\nTypes supported: NumberParameter, StringParameter, ChoiceParameter"
    )


class GroupParametersWidget(QGroupBox):

    """Class for a group of parameters.
    """

    def __init__(self, parameters: GroupParameters, parent=None):
        """Initialize the GroupParametersWidget

        Parameters
        ----------
        parameters : GroupParameters
            The group of parameters managed by this widget
        parent : QWidget, optional
            The widget parent
        """
        super().__init__(parent)
        self.group_parameters: GroupParameters = parameters
        self.widgetsParameters: dict[str, ParameterWidget] = dict()
        self.setTitle(self.group_parameters.name)
        main_layout = QVBoxLayout()
        for parameter in self.group_parameters.parameters.values():
            param_widget = create_parameter_widget(parameter, self)
            self.widgetsParameters[parameter.name] = param_widget
            main_layout.addWidget(param_widget)
        self.setLayout(main_layout)

    def get_value(self, param_name: str) -> Any:
        """Returns the value of a parameter

        Parameters
        ----------
        param_name : str
            The name of the parameter to be retrieved

        Returns
        -------
        Any
            The current value of this parameter
        """
        return self.widgetsParameters[param_name].get_value()

    def to_dict(self) -> dict[str, Any]:
        """Export all parameter data in a dictionary

        Returns
        -------
        dict[str, Any]
            Dictionary with parameter names in key and 
            the parameter values in value.
        """
        return {
            name: widget.get_value()
            for name, widget in self.widgetsParameters.items()
        }
