# Resource object code (Python 3)
# Created by: object code
# Created by: The Resource Compiler for Qt version 6.2.2
# WARNING! All changes made in this file will be lost!

from PySide6 import QtCore

qt_resource_data = b"\
\x00\x00\x01\x09\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 24 24\x22 width\
=\x2248\x22 height=\x2248\
\x22><path fill=\x22no\
ne\x22 d=\x22M0 0h24v2\
4H0z\x22/><path d=\x22\
M19.376 12.416L8\
.777 19.482A.5.5\
 0 0 1 8 19.066V\
4.934a.5.5 0 0 1\
 .777-.416l10.59\
9 7.066a.5.5 0 0\
 1 0 .832z\x22 fill\
=\x22rgba(0,0,0,1)\x22\
/></svg>\
\x00\x00\x00\xbd\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 24 24\x22 width\
=\x2248\x22 height=\x2248\
\x22><path fill=\x22no\
ne\x22 d=\x22M0 0h24v2\
4H0z\x22/><path d=\x22\
M6 5h2v14H6V5zm1\
0 0h2v14h-2V5z\x22 \
fill=\x22rgba(0,0,0\
,1)\x22/></svg>\
\x00\x00\x01\x18\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 24 24\x22 width\
=\x2248\x22 height=\x2248\
\x22><path fill=\x22no\
ne\x22 d=\x22M0 0h24v2\
4H0z\x22/><path d=\x22\
M16 12.667L5.777\
 19.482A.5.5 0 0\
 1 5 19.066V4.93\
4a.5.5 0 0 1 .77\
7-.416L16 11.333\
V5a1 1 0 0 1 2 0\
v14a1 1 0 0 1-2 \
0v-6.333z\x22 fill=\
\x22rgba(0,0,0,1)\x22/\
></svg>\
\x00\x00\x01\x15\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 24 24\x22 width\
=\x2248\x22 height=\x2248\
\x22><path fill=\x22no\
ne\x22 d=\x22M0 0h24v2\
4H0z\x22/><path d=\x22\
M8 11.333l10.223\
-6.815a.5.5 0 0 \
1 .777.416v14.13\
2a.5.5 0 0 1-.77\
7.416L8 12.667V1\
9a1 1 0 0 1-2 0V\
5a1 1 0 1 1 2 0v\
6.333z\x22 fill=\x22rg\
ba(0,0,0,1)\x22/></\
svg>\
\x00\x00\x00\xe6\
<\
svg xmlns=\x22http:\
//www.w3.org/200\
0/svg\x22 viewBox=\x22\
0 0 24 24\x22 width\
=\x2248\x22 height=\x2248\
\x22><path fill=\x22no\
ne\x22 d=\x22M0 0h24v2\
4H0z\x22/><path d=\x22\
M6 5h12a1 1 0 0 \
1 1 1v12a1 1 0 0\
 1-1 1H6a1 1 0 0\
 1-1-1V6a1 1 0 0\
 1 1-1z\x22 fill=\x22r\
gba(0,0,0,1)\x22/><\
/svg>\
"

qt_resource_name = b"\
\x00\x08\
\x02\x8f\x89{\
\x00p\
\x00l\x00a\x00y\x00b\x00a\x00c\x00k\
\x00\x04\
\x00\x07r\x89\
\x00p\
\x00l\x00a\x00y\
\x00\x05\
\x00v\x8c\x95\
\x00p\
\x00a\x00u\x00s\x00e\
\x00\x0c\
\x02-\xde\xe4\
\x00s\
\x00k\x00i\x00p\x00-\x00f\x00o\x00r\x00w\x00a\x00r\x00d\
\x00\x09\
\x003s\xdb\
\x00s\
\x00k\x00i\x00p\x00-\x00b\x00a\x00c\x00k\
\x00\x04\
\x00\x07\xab`\
\x00s\
\x00t\x00o\x00p\
"

qt_resource_struct = b"\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x01\x00\x00\x00\x01\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x00\x00\x02\x00\x00\x00\x05\x00\x00\x00\x02\
\x00\x00\x00\x00\x00\x00\x00\x00\
\x00\x00\x00\x16\x00\x00\x00\x00\x00\x01\x00\x00\x00\x00\
\x00\x00\x01|\xd5\x9eMp\
\x00\x00\x00j\x00\x00\x00\x00\x00\x01\x00\x00\x04\x03\
\x00\x00\x01|\xd5\x9eMp\
\x00\x00\x00R\x00\x00\x00\x00\x00\x01\x00\x00\x02\xea\
\x00\x00\x01|\xd5\x9eMp\
\x00\x00\x00$\x00\x00\x00\x00\x00\x01\x00\x00\x01\x0d\
\x00\x00\x01|\xd5\x9eMp\
\x00\x00\x004\x00\x00\x00\x00\x00\x01\x00\x00\x01\xce\
\x00\x00\x01|\xd5\x9eMp\
"

def qInitResources():
    QtCore.qRegisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

def qCleanupResources():
    QtCore.qUnregisterResourceData(0x03, qt_resource_struct, qt_resource_name, qt_resource_data)

qInitResources()
