from enum import Enum
# to load (not use orjson because it hasn't object_hook)
# And the speed is less important than during the calculation
import json
import orjson  # to fast dump for calculation

from QPySimu.config import SimulationConfig
from QPySimu.items import ItemDisplayable
from QPySimu.core import SimuState
from QPySimu.errors import ItemClassNotFound


CLASS_DISPLAYABLE_ITEMS = {}


def add_item_types(item_types: list[type]):
    """Method to add a subclass list of ItemDisplayable
    in CLASS_DISPLAYABLE_ITEMS.

    !!! info
        Use to save and load custom item.
        This is probably not the best method.

    !!! warning

        This function does not raise an exception,
        but filters all types that are not subclasses of ItemDisplayable.

    Parameters
    ----------
    item_types : list[type]
        List of type (subclass of ItemDisplayable).
    """
    global CLASS_DISPLAYABLE_ITEMS
    CLASS_DISPLAYABLE_ITEMS.update({
        type_.__name__: type_
        for type_ in item_types
        if issubclass(type_, ItemDisplayable)
    })


class TypesSerializable(Enum):

    """Enumeration of Serializable types

    Attributes
    ----------
    SimulationConfig : int
        SimulationConfig class
    SimuState : int
        SimuState class
    ItemDisplayable : int
        ItemDisplayable class
    """

    SimulationConfig = 1
    SimuState = 2
    ItemDisplayable = 3


def _default(obj: object) -> dict:
    """Serializes objects that have a type of the enum TypesSerializable

    Parameters
    ----------
    obj : object
        The object to serialize

    Returns
    -------
    dict
        The dictionary that represents the object
    """
    if isinstance(obj, SimulationConfig):
        serialized = {'type': TypesSerializable.SimulationConfig}
        serialized.update(obj.to_dict())
        return serialized

    elif isinstance(obj, SimuState):
        serialized = {'type': TypesSerializable.SimuState}
        serialized.update(obj.to_dict())
        return serialized

    elif isinstance(obj, ItemDisplayable):
        serialized = {
            'type': TypesSerializable.ItemDisplayable,
            'item_class': obj.__class__.__name__
        }
        serialized.update(obj.to_dict())
        return serialized


def _object_hook(obj) -> object:
    """Object hook function to deserialize objects

    Parameters
    ----------
    obj : TYPE
        The JSON object to deserialize

    Returns
    -------
    object
        The deserialized object
    """
    type_ = obj.get('type')
    if type_ is not None:
        obj.pop('type')

    if type_ == TypesSerializable.SimulationConfig.value:
        return SimulationConfig.from_dict(obj)

    elif type_ == TypesSerializable.ItemDisplayable.value:
        return CLASS_DISPLAYABLE_ITEMS[obj['item_class']].from_dict(obj)

    elif type_ == TypesSerializable.SimuState.value:
        return SimuState.from_dict(obj)
    return obj


def save(path: str, obj: object):
    """Function to save a object in a json file.

    Parameters
    ----------
    path : str
        The path of json file
    obj : object
        The object to save
    """
    options = orjson.OPT_APPEND_NEWLINE | orjson.OPT_INDENT_2 \
        | orjson.OPT_NON_STR_KEYS
    obj_serialized = orjson.dumps(obj, option=options, default=_default)
    with open(path, 'wb') as f:
        f.write(obj_serialized)


def load(path: str) -> object:
    """Function to load objects from json file.

    Parameters
    ----------
    path : str
        The path of json file

    Returns
    -------
    object
        The loaded object
    """
    with open(path, 'r') as f:
        obj = json.load(f, object_hook=_object_hook)
    return obj
