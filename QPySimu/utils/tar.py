import os
import tarfile


def create_archive(path: str, files_to_archive: list[str], dir_: str = '.'):
    """Function to create an archive with a list of files.
    The files must be in the same directory.

    Parameters
    ----------
    path : str
        The path of archive
    files_to_archive : list[str]
        The list of files
    dir_ : str, optional
        The directory path of files
    """
    cwd = os.path.abspath(os.getcwd())
    path = os.path.abspath(path)
    os.chdir(dir_)
    with tarfile.open(path, 'w:gz') as tar:
        for file in files_to_archive:
            tar.add(file)
    os.chdir(cwd)


def extract_archive(path: str, dest: str):
    """Function to extract all files from an archive.

    Parameters
    ----------
    path : str
        The path of archive
    dest : str
        The path of the directory destination.
    """
    with tarfile.open(path, 'r:gz') as tar:
        tar.extractall(dest)
