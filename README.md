# QPySimu

QPySimu is a Python package to easily create GUIs with [PySide6](https://doc.qt.io/qtforpython-6/) for simulations. This package includes:

* Base for the graphical interface
* Simulation worker
* Some graphic item
* Configuration of simulation parameters
* Filter of items in view



## Installation

To install this package from the repository, use:

```bash
pip install git+https://gitlab.com/LostPy/qpysimu.git
```

To install optional requirements (for the local documentation for example), use:

```bash
pip install "QPySimu[doc] @ git+https://gitlab.com/LostPy/qpysimu.git"
```

## Documentation

The documentation is avaible in local with mkdocs.

1. To install the documentation requirements (not necessary if the package has been installed with the `doc` option), in the same environment as the package:

```bash
python -m QPySimu --install-doc
```

2. To run a local documentation server:

```bash
python -m QPySimu --doc
```

3. Open the local URL displayed in your terminal to see the documentation.

```bash
Serving on http://127.0.0.1:8000/
```

## Getting Started



## License

This package is under [MIT license][mit]:

> Copyright © 2022 - LostPy  
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:  
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.  
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[mit]: https://mit-license.org/
