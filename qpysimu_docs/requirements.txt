mkdocs==1.2.3
mkdocs-material==8.1.1
mkdocs-i18n==0.4.2
mkdocstrings[numpy-style]==0.16.2
pytkdocs[numpy-style]==0.12.0
pyside6==6.2.2
orjson==3.6.6