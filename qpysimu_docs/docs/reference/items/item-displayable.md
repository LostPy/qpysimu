# ItemDisplayable

Class to represent an item displayable during the simulation with:

 * Attributes evolving during the simulation
 * An associated QGraphicsItem

It's recommended to inherit your displayable items from this class.

??? example

    ```python
    from PySide6.QtWidgets import QGraphicsRectItem
    from PySide6.QtGui import QColor, QBrush
    from QPySimu.items import ItemDisplayable


    class RedRectItem(ItemDisplayable):

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.width = 200
            self.height = 150

        def default_graphics_item(self) -> QGraphicsRectItem:
            rect = QGraphicsRectItem(0, 0, self.width, self.height)
            rect.setBrush(QBrush(QColor('red')))
            return rect
    ```

## `#!python __init__(self, init_pos, is_visible)`

:::QPySimu.items.ItemDisplayable.__init__

## `#!python id : int` `property`

:::QPySimu.items.ItemDisplayable.id

## `#!python graphics_item : QGraphicsItem` `property`

:::QPySimu.items.ItemDisplayable.graphics_item

## `#!python position : QPointF` `property`

:::QPySimu.items.ItemDisplayable.position

## `#!python x : float` `property`

:::QPySimu.items.ItemDisplayable.x

## `#!python y : float` `property`

:::QPySimu.items.ItemDisplayable.y

## `#!python is_visible : float` `property`

:::QPySimu.items.ItemDisplayable.is_visible

## `#!python to_dict`

:::QPySimu.items.ItemDisplayable.to_dict

## `#!python klass_is_visible` `classmethod`

:::QPySimu.items.ItemDisplayable.klass_is_visible

## `#!python set_klass_visible` `classmethod`

:::QPySimu.items.ItemDisplayable.set_klass_visible

## `#!python default_graphics_item` `classmethod`

:::QPySimu.items.ItemDisplayable.default_graphics_item