# items package

This package defines graphical objects that can be used in a simulation.

## Subpackage

 * [2d](./2d/index.md)
 * [3d](./3d/index.md)

## Class

 * [ItemDisplayable](./item-displayable.md)
