# SimulationResult

Class to read the result of a simulation. It's a subclass of QThread.

### `#!python mode : SimulationMode` `property`

:::QPySimu.core.SimulationResult.mode

### `#!python __init__(self, save_file, frequency, parent)` `special`

:::QPySimu.core.SimulationResult.__init__

### `#!python start(self, priority)`

:::QPySimu.core.SimulationResult.start

### `#!python on_finished(self)`

:::QPySimu.core.SimulationResult.on_finished

### `#!python run(self)`

:::QPySimu.core.SimulationResult.run

### `#!python step(self)`

:::QPySimu.core.SimulationResult.step

### `#!python setMode(self, mode)`

:::QPySimu.core.SimulationResult.setMode

### `#!python next(self)`

:::QPySimu.core.SimulationResult.next

### `#!python previous(self)`

:::QPySimu.core.SimulationResult.previous


## SimulationMode

:::QPySimu.core.SimulationMode
