# core package

This package defines the non-graphical objects used to run the simulations.

## Class

 * [Simulation](./simulation.md)
 * [SimulationResult](./simulation-result.md)
 * [SimuState](./simu-state.md)