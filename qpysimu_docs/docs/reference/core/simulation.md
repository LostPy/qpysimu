# Simulation

Class to define a simulation, with a repeat algorithm for a given duration with a given time step.

It's a subclass of QThread.

!!! note

    It is recommended to create a subclass of Simulation by redefining the step method which should contain the simulation algorithm.

    However, it is possible to write the algorithm in a function and pass this function at the creation of the Simulation instance using the 'step_function' argument.

!!! info

    The function containing the algorithm and the step method must take an instance of SimuState as argument (state t-1) and returns an instance of SimuState (state t).

??? example "Example of subclass"

    ```python
    class MySimulation(Simulation)

        def __init__(self, time: int, dt: float, **kwargs):
            super().__init__(time, dt, **kwargs)

        def step(self):
            for item in self.state.displayable_items.values():
                # move item
                item.x += 2
                item.y += 2
    ```

## `#!python __init__(self, time, dt, step_function, initial_state, save_file, parent)`

:::QPySimu.core.Simulation.__init__


## `#!python start(self, config, priority)`

:::QPySimu.core.Simulation.start

## `#!python on_finished(self)`

:::QPySimu.core.Simulation.on_finished

## `#!python run(self)`

:::QPySimu.core.Simulation.run

## `#!python step(self)`

:::QPySimu.core.Simulation.step
