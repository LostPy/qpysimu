# Reference

!!! warning

    This package is under active development, there is not yet a version ready for use.


## Package Structure

 * [config](./config/index.md)
    - [Config](./config/config.md)
    - [AppConfig](./config/appConfig.md)
    - [SimuInterfaceConfig](./config/simuInterfaceConfig.md)
    - [SimulationConfig](./config/simulationConfig.md)
    - [BaseParameter](./config/parameter.md#baseparameter)
        - [Parameter](./config/parameter.md#parameter)
            - [NumberParameter](./config/parameter.md#numberparameter)
            - [StringParameter](./config/parameter.md#stringparameter)
            - [ChoiceParameter](./config/parameter.md#choiceparameter)
        - [GroupParameters](./config/parameter.md#groupparameters)
    - [create_parameter](./config/parameter.md#create_parameter)
 * [core](./core/index.md)
    - [Simulation](./core/simulation.md)
    - [SimulationResult](./core/simulation-result.md)
    - [SimuState](./core/simu-state.md)
 * [gui](./gui/index.md)
    - [widgets](./gui/widgets/index.md)
        - [IemFilterWidget](./gui/widgets/item-filter.md)
        - [ParameterWidget](./gui/widgets/parameter.md#parameterwidget)
            - [NumberParameterWidget](./gui/widgets/parameter.md#numberparameterwidget)
            - [StringParameterWidget](./gui/widgets/parameter.md#stringparameterwidget)
            - [ChoiceParameterWidget](./gui/widgets/parameter.md#choiceparameterwidget)
        - [GroupParametersWidget](./gui/widgets/parameter.md#groupparameterswidget)
        - [create_parameter_widget](./gui/widgets/parameter.md#create_parameter_widget)
    - [BaseSimuWindow](./gui/base-simu-window.md)
    - [SimuWindow2D](./gui/simu-window-2d.md)
    - [SimuWindow3D](./gui/simu-window-3d.md)
    - [SimulationScene](./gui/simulation-scene.md)
    - [SimulationView](./gui/simulation-view.md)
    - [AboutDialog](./gui/about.md)
 * [items](./items/index.md)
 * [errors](./errors.md)