# Errors

Name|Parent|Description
:--:|:----:|-----------
`QPySimuException`|`Exception`|Base class for exception of QPySimu package
`ConfigExeption`|`QPySimuException`|Exception raised when a error was found during the parse of config file.
`ParamTypeUnsupported`|`ParamTypeUnsupported`|Exception raised when the a type of parameter is not supported.
`ItemClassNotFound`|`QPySimuException`|Exception raised while reading a json file containing an object of unknown class (class not present in the "CLASS_DISPLAYABLE_ITEMS" list).