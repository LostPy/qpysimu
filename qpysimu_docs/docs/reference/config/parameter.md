# Parameters class

## BaseParameter

:::QPySimu.config.parameter.BaseParameter

## Parameter

:::QPySimu.config.parameter.Parameter

## NumberParameter

:::QPySimu.config.parameter.NumberParameter

## StringParameter

:::QPySimu.config.parameter.StringParameter

## ChoiceParameter

:::QPySimu.config.parameter.ChoiceParameter

## GroupParameters

:::QPySimu.config.parameter.GroupParameters

## Functions

### create_parameter

:::QPySimu.config.parameter.create_parameter
