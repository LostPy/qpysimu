# config package

This package defines all config class and generic parameters of simulation.

## Class

 * [Config](./config.md)
 * [AppConfig](./appConfig.md)
 * [SimuInterfaceConfig](./simuInterfaceConfig.md)
 * [SimulationConfig](./simulationConfig.md)
 * [BaseParameter](./parameter.md#baseparameter)
 * [Parameter](./parameter.md#parameter)
 * [NumberParameter](./parameter.md#numberparameter)
 * [StringParameter](./parameter.md#stringparameter)
 * [ChoiceParameter](./parameter.md#choiceparameter)
 * [GroupParameters](./parameter.md#groupparameters)

## Functions

 * [create_parameter](./parameter.md#create_parameter)