# SimulationScene

Subclass of QGraphicsScene to define the simulation scene.

### `#!python __init__(self, config_file, items_class, parent)` `special`

:::QPySimu.gui.SimulationScene.__init__

### `#!python newState(self, state)`

:::QPySimu.gui.SimulationScene.newState
