# AboutDialog

A default simple About window.

This window displays:
  * The application's name
  * The application's description
  * Authors
  * License
  * Repository url
  * Site url
  * Year of release

!!! info

    Each information is displayed if the value is not None

## `#!python __init__(self, app_config, parent)`

:::QPySimu.gui.AboutDialog.__init__