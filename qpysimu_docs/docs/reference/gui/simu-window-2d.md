# SimuWindow2D

MainWindow for a simulation in a 2D space. It's a subclass of [BaseSimuWindow](./base-simu-window.md).

??? example

	```py
	import sys
	from PySide6.QtWidgets import QApplication
	from QPySimu.gui import SimuWindow2D

	app = QApplication(sys.argv)
	w = SimuWindow2D("simu_config.yml")
	w.show()
	sys.exit(app.exec())
	```

## Methods overwrite

### `#!python __init__(self, config_file, items_class, parent)` `special`

:::QPySimu.gui.SimuWindow2D.__init__

### `#!python init_view(self)`

:::QPySimu.gui.SimuWindow2D.init_view

### `#!python connections(self)`

:::QPySimu.gui.SimuWindow2D.connections

### `#!python on_simu_result_started(self)`

:::QPySimu.gui.SimuWindow2D.on_simu_result_started

### `#!python on_simu_result_progressed(self, t, time, progress, state)`

:::QPySimu.gui.SimuWindow2D.on_simu_result_progressed
