# gui package

This package contains the widgets and mainwindows allowing to interact with the simulation.

## subpackages

 * [widgets](./widgets/index.md)

## Class

 * [AboutDialog](./about.md)
 * [BaseSimuWindow](./base-simu-window.md)
 * [SimuWindow2D](./simu-window-2d.md)
 * [SimuWindow3D](./simu-window-3d.md)
 * [SimulationScene](./simulation-scene.md)
 * [SimulationView](./simulation-view.md)
