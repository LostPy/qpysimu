# Parameters Widgets

!!! info

	This module contains all widget for parameters and groups parameters

## ParameterWidget

Base class for parameter widgets. A widget to allow the modification of a parameter value by the user.

!!! warning

    This class should not be used, you can directly use 
    the derived class corresponding to the parameter type.

**Attributes:**

Name|Type|Description
:--:|:--:|-----------
`parameter`|`QPySimu.config.Parameter`|The parameter for this widget
`nameLabel`|`PySide6.QtWidgets.QLabel`|The label displaying the name of the parameter
`valueWidget`|`PySide6.QtWidgets.QWidget`|The input widget to change the value

### `#!python __init__(self, parameter, parent)`

:::QPySimu.gui.widgets.ParameterWidget.__init__

### `#!python get_value(self)`

:::QPySimu.gui.widgets.ParameterWidget.get_value

## NumberParameterWidget

Class for a parameter of type 'number' (float or int).

**Class Attributes:**

Name|Type|Description|Default
:--:|:--:|-----------|:-----:
`DEFAULT_MINIMUM`|`int`|The default minimum value of the input widget|`#!python -999 999`
`DEFAULT_MAXIMUM`|`int`|The default maximum value of the input widget|`#!python 999 999`

### `#!python __init__(self, parameter, parent)`

:::QPySimu.gui.widgets.NumberParameterWidget.__init__

### `#!python get_value(self)`

:::QPySimu.gui.widgets.NumberParameterWidget.get_value

## StringParameterWidget

Class for a parameter of type 'string' (str).

**Class Attributes:**

Name|Type|Description|Default
:--:|:--:|-----------|:-----:
`AUTO_STRIP`|`bool`|If True, the `get_value` method returns `text.strip()`|`#!python True`

### `#!python __init__(self, parameter, parent)`

:::QPySimu.gui.widgets.StringParameterWidget.__init__

### `#!python get_value(self)`

:::QPySimu.gui.widgets.StringParameterWidget.get_value

## ChoiceParameterWidget

Class for a parameter of type 'string' (str) with a list of choices.

### `#!python __init__(self, parameter, parent)`

:::QPySimu.gui.widgets.ChoiceParameterWidget.__init__

### `#!python get_value(self)`

:::QPySimu.gui.widgets.ChoiceParameterWidget.get_value

## GroupParametersWidget

Class for a group of parameters.

**Attributes:**

Name|Type|Description
:--:|:--:|-----------
`group_parameters`|`QPySimu.config.GroupParameters`|The group parameters for this widget
`widgetsParameters`|`dict[str, ParameterWidget]`|A dictionary with parameter names as keys and the parameter widgets as values.

### `#!python __init__(self, parameters, parent)`

:::QPySimu.gui.widgets.GroupParametersWidget.__init__

### `#!python get_value(self, param_name)`

:::QPySimu.gui.widgets.GroupParametersWidget.get_value

### `#!python to_dict(self)`

:::QPySimu.gui.widgets.GroupParametersWidget.to_dict

## `#!python create_parameter_widget(parameter, parent)`

:::QPySimu.gui.widgets.create_parameter_widget