# widgets package

This subpackage contains all custom widgets used in the gui.

## Widgets list

 * [ItemFilterWidget](./item-filter.md)
 * [ParameterWidget](./parameter.md#parameterwidget)
 * [NumberParameterWidget](./parameter.md#numberparameterwidget)
 * [StringParameterWidget](./parameter.md#stringparameterwidget)
 * [ChoiceParameterWidget](./parameter.md#choiceparameterwidget)
 * [GroupParametersWidget](./parameter.md#groupparameterswidget)

## Functions

 * [create_parameter_widget](./parameter.md#create_parameter_widget)
 