# ItemFilterWidget

Subclass of QCheckBox to filter a type of item.

### `#!python __init__(self, item_type, parent)`

:::QPySimu.gui.widgets.ItemFilterWidget.__init__

### `#!python on_stateChanged(self, state)`

:::QPySimu.gui.widgets.ItemFilterWidget.on_stateChanged
