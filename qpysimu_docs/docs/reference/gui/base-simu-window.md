# BaseSimuWindow

Base class for main window. This window contains:

  * The initialized parameters area
  * The item filter
  * The widgets used for the calculation of the simulation
  * The widgets used for reading the results.

!!! warning

    This window does not manage the graphical views.


### `#!python __init__(self, config_file, items_class, parent)`

:::QPySimu.gui.BaseSimuWindow.__init__

## Initialization

### `#!python init_icons(self)`

:::QPySimu.gui.BaseSimuWindow.init_icons

### `#!python init_app(self, config_file)`

:::QPySimu.gui.BaseSimuWindow.init_app

### `#!python init_parameters(self)`

:::QPySimu.gui.BaseSimuWindow.init_parameters

### `#!python init_filter(self, items_class)`

:::QPySimu.gui.BaseSimuWindow.init_filter


## Other methods

### `#!python setStopEnable(self, enable)`

:::QPySimu.gui.BaseSimuWindow.setStopEnable

### `#!python setStepEnable(self, enable)`

:::QPySimu.gui.BaseSimuWindow.setStepEnable

### `#!python run_to_stepbystep(self)`

:::QPySimu.gui.BaseSimuWindow.run_to_stepbystep

### `#!python setSimulation(self, simulation)`

:::QPySimu.gui.BaseSimuWindow.setSimulation

## Slots

### `#!python on_simu_result_modeChanged(self, mode)`

:::QPySimu.gui.BaseSimuWindow.on_simu_result_modeChanged

### `#!python on_simu_result_finished(self)`

:::QPySimu.gui.BaseSimuWindow.on_simu_result_finished

### `#!python on_simu_result_progressed(self, t, time, progress, state)`

:::QPySimu.gui.BaseSimuWindow.on_simu_result_progressed

### `#!python on_simulation_finished(self)`

:::QPySimu.gui.BaseSimuWindow.on_simulation_finished

### `#!python on_simulation_progressed(self, t, progress)`

:::QPySimu.gui.BaseSimuWindow.on_simulation_progressed

### `#!python on_buttonHideParam_clicked(self)`

:::QPySimu.gui.BaseSimuWindow.on_buttonHideParam_clicked

### `#!python on_checkBoxStepByStep_stateChanged(self, state)`

:::QPySimu.gui.BaseSimuWindow.on_checkBoxStepByStep_stateChanged

### `#!python on_sliderFrequency_valueChanged(self, value)`

:::QPySimu.gui.BaseSimuWindow.on_sliderFrequency_valueChanged

### `#!python on_actionCalculate_triggered(self)`

:::QPySimu.gui.BaseSimuWindow.on_actionCalculate_triggered

### `#!python on_actionPlay_triggered(self)`

:::QPySimu.gui.BaseSimuWindow.on_actionPlay_triggered

### `#!python on_actionStop_triggered(self)`

:::QPySimu.gui.BaseSimuWindow.on_actionStop_triggered

### `#!python on_actionNext_triggered(self)`

:::QPySimu.gui.BaseSimuWindow.on_actionNext_triggered

### `#!python on_actionPrevious_triggered(self)`

:::QPySimu.gui.BaseSimuWindow.on_actionPrevious_triggered

### `#!python on_actionAbout_triggered(self)`

:::QPySimu.gui.BaseSimuWindow.on_actionAbout_triggered

### `#!python on_actionQuit_triggered(self)`

:::QPySimu.gui.BaseSimuWindow.on_actionQuit_triggered

## Events

### `#!python closeEvent(self, event)`

:::QPySimu.gui.BaseSimuWindow.closeEvent