# SimulationView

Subclass of QGraphicsView to define the main view of GUI.

**Class Attributes:**

Name|Type|Description
:--:|:--:|-----------
`ZOOM_SENSIBILITY`|`float`|The zoom sensibility. Must be between 0 and 1.
`ZOOM_ENABLE`|`bool`|If True, enable the zoom feature.

### `#!python __init__(self, scene, parent)` `special`

:::QPySimu.gui.SimulationView.__init__

### `#!python zoom(self, factor)`

:::QPySimu.gui.SimulationView.zoom

### `#!python wheelEvent(self, event)`

:::QPySimu.gui.SimulationView.wheelEvent
