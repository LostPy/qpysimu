site_name: QPySimu
repo_url: https://gitlab.com/lostpy/qpysimu
edit_uri: ''
site_description: QPySimu is a Python package to easily create GUIs with [PySide6](https://doc.qt.io/qtforpython-6/) for simulations.

markdown_extensions:
  - admonition
  - pymdownx.details
  - pymdownx.superfences
  - pymdownx.highlight
  - pymdownx.inlinehilite
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.tasklist:
      custom_checkbox: true

plugins:
  - mkdocstrings:
      default_handler: python
      handlers:
        python:
          rendering:
            show_source: true
          selection:
            docstring_style: numpy
            filters:
              - "!^__"
              - "^__init__$"

theme:
  name: material
  language: en
  palette:
    - scheme: slate
      primary: cyan
      accent: cyan
      toggle:
        icon: material/weather-sunny
        name: Switch to light mode
    - scheme: default
      primary: cyan
      accent: cyan
      toggle:
        icon: material/weather-night
        name: Switch to dark mode
  features:
    - navigation.tabs
    - content.tabs.link
    - toc.integrate
    - navigation.top

extra_css:
  - extra/css/tables.css

extra_javascript:
  - extra/javascripts/tabLinked.js

nav:
  - Home: index.md
  - Getting Started: getting-started.md
  - Reference:
    - reference/index.md
    - config:
      - reference/config/index.md
      - Config: reference/config/config.md
      - AppConfig: reference/config/appConfig.md
      - SimulationConfig: reference/config/simulationConfig.md
      - SimuInterfaceConfig: reference/config/simuInterfaceConfig.md
      - Parameters: reference/config/parameter.md
    - core:
      - reference/core/index.md
      - Simulation: reference/core/simulation.md
      - SimulationResult: reference/core/simulation-result.md
      - SimuState: reference/core/simu-state.md
    - items:
      - reference/items/index.md
      - ItemDisplayable: reference/items/item-displayable.md
      - 2D items: reference/items/2d/index.md
      - 3D items: reference/items/3d/index.md
    - gui:
      - reference/gui/index.md
      - Widgets:
        - reference/gui/widgets/index.md
        - ItemFilterWidget: reference/gui/widgets/item-filter.md
        - ParameterWidget: reference/gui/widgets/parameter.md
      - BaseSimuWindow: reference/gui/base-simu-window.md
      - SimuWindow2D: reference/gui/simu-window-2d.md
      - SimuWindow3D: reference/gui/simu-window-3d.md
      - SimulationScene: reference/gui/simulation-scene.md
      - SimulationView: reference/gui/simulation-view.md
      - About: reference/gui/about.md
    - errors: reference/errors.md
